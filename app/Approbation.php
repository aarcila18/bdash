<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Approbation extends Model
{
    //
    public function approbable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
