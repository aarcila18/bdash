<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Observations extends Model
{
    //
    public function observable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
