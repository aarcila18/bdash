<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkforceProduct extends Model
{
    //
    protected $table = 'product_workforce';

    public function production()
    {
        return $this->belongsTo(Production::class);
    }
}
