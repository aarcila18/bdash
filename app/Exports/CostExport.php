<?php

namespace App\Exports;

use App\Cost;
use Maatwebsite\Excel\Concerns\FromCollection;

class CostExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Cost::all();
    }
}
