<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataSheet extends Model
{
    //
    public function observations()
    {
        return $this->morphMany('App\Observations', 'observable');
    }
}
