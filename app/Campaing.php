<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaing extends Model
{
    //
    public function costs()
    {
        return $this->hasMany(Cost::class);
    }

    public function documents()
    {
        return $this->hasMany(Document::class);
    }

    public function outgoings()
    {
        return $this->hasMany(Outgoing::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function sales()
    {
        return $this->hasMany(Sale::class);
    }

    public function socialtracks()
    {
        return $this->hasMany(SocialTrack::class);
    }

    public function observations()
    {
        return $this->morphMany('App\Observations', 'observable');
    }
}
