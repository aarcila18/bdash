<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cost extends Model
{
    //
    public function production()
    {
        return $this->belongsTo(Production::class);
    }

    public function campaing()
    {
        return $this->belongsTo(Campaing::class);
    }

    public function observations()
    {
        return $this->morphMany('App\Observations', 'observable');
    }

    public function attachments()
    {
        return $this->morphMany('App\Attachment', 'attachable');
    }
}
