<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    public function parent()
    {
        return $this->belongsTo(Product::class, 'parent_sku', 'sku');
    }

    public function variations()
    {
        return $this->hasMany(Product::class, 'parent_sku', 'sku');
    }

    public function campaing()
    {
        return $this->belongsTo(Campaing::class);
    }

    public function production()
    {
        return $this->belongsTo(Production::class);
    }

    public function observations()
    {
        return $this->morphMany('App\Observations', 'observable');
    }

    public function sales(){
        return $this->belongsToMany(
            Sale::class,
            "products_sales",
            'product_sku',
            'sale_id',
            'sku',
            'id');
    }

    public function materials()
    {
        return $this->belongsToMany(Material::class)->withPivot('id','material_quantity','material_cost','quantity','production_id')->withTimestamps();
    }

    public function workforces()
    {
        return $this->belongsToMany(Workforce::class)->withPivot('id','workforce_quantity','workforce_cost','quantity','production_id')->withTimestamps();
    }

    public function attachments()
    {
        return $this->morphMany('App\Attachment', 'attachable');
    }
}
