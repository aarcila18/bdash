<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function observations()
    {
        return $this->morphMany('App\Observations', 'observable');
    }

    public function board()
    {
        return $this->belongsTo(Board::class);
    }
}
