<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Production extends Model
{
    //
    public function costs()
    {
        return $this->hasMany(Cost::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function productsMaterials()
    {
        return $this->hasMany(MaterialProduct::class);
    }

    public function productsWorkforces()
    {
        return $this->hasMany(WorkforceProduct::class);
    }

    public function observations()
    {
        return $this->morphMany('App\Observations', 'observable');
    }

    public function attachments()
    {
        return $this->morphMany('App\Attachment', 'attachable');
    }
}
