<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Material;
use App\Provider;
//notificacion
use App\User;
use App\Notifications\GenericNotify;
use Notification;

class AllMaterials extends Component
{
    public $search,$name,$cost,$quantity,$provider_id;

    public function render()
    {
        $providers = Provider::orderBy("created_at","desc")->get();
        $materials = Material::orderBy("created_at","desc")->where('name',"like", "%".$this->search."%")->paginate(20);
        return view('livewire.all-materials',
        [
            'materials'=>$materials,
            'providers'=>$providers
        ]);
    }

    public function create(){

        $this->validate([
            'name' => 'required|min:6',
            'cost' => 'required|numeric|min:0|not_in:0',
            'quantity' => 'required|numeric|min:0|not_in:0',
            'provider_id' => 'required|exists:providers,id',
        ]);

        $material= new material();
        $material->name = $this->name;
        $material->cost = $this->cost;
        $material->quantity = $this->quantity;
        $material->unit_cost = $this->cost/$this->quantity;
        $material->provider_id = $this->provider_id;
        $material->save();

        // notificacion y alerta
        $success_message="nuevo material añadido";
        
        $users = User::whereHas('roles', function ($query) {
            $query->where('name', 'admin');
        })->get();

        $data = [ 'title' => $success_message,'body' => $material->name ];
        Notification::send($users, new GenericNotify($data));

        session()->flash('message', $success_message);

        $this->reset();
    }
}
