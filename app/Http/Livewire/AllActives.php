<?php

namespace App\Http\Livewire;

use Livewire\Component;

class AllActives extends Component
{
    public function render()
    {
        return view('livewire.all-actives');
    }
}
