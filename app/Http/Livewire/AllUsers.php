<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use App\Observations;
//notificacion
use App\User;
use App\Role;
use App\Notifications\GenericNotify;
use Notification;

class AllUsers extends Component
{
    public $search, $name, $email, $password, $edit_user,$role;

    public function render()
    {
        $roles = Role::all();
        $users = User::orderBy("created_at", "desc")->where('name', "like", "%" . $this->search . "%")->paginate(20);
        return view(
            'livewire.all-users',
            [
                'users' => $users,
                'roles' => $roles,
            ]
        );
    }

    public function create()
    {

        $this->validate([
            'name' => 'required|min:6',
            'email' => 'required',
            'password' => 'required',
        ]);

        $user = new User();
        $user->name = $this->name;
        $user->email = $this->email;
        $user->password = Hash::make($this->password);
        $user->save();

        $user->roles()->attach(Role::find($this->role)->first());

        // notificacion y alerta
        $success_message = "nuevo usuario añadido";

        $users = User::whereHas('roles', function ($query) {
            $query->where('name', 'admin');
        })->get();

        $data = ['title' => $success_message, 'body' => $user->name];
        Notification::send($users, new GenericNotify($data));

        session()->flash('message', $success_message);

        $this->reset();
    }
}
