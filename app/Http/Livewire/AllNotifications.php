<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Auth;

class AllNotifications extends Component
{
    public function render()
    {
        return view('livewire.all-notifications',[
            'unreadNotifications' => Auth::user()->unreadNotifications()->paginate(10),
            'readNotifications' => Auth::user()->readNotifications()->paginate(10),
        ]);
    }

    public function markAsRead($id)
    {
        Auth::user()->unreadNotifications->where('id', $id)->markAsRead();
    }
}
