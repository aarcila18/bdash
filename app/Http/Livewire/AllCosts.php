<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Cost;
//notificacion
use App\User;
use App\Notifications\GenericNotify;
use Notification;

class AllCosts extends Component
{
    public $search,$name,$value,$type,$productionId;
    
    public function mount($productionId){
        $this->productionId = $productionId;
    }

    public function render()
    {
        if($this->productionId==null){
            $costs = Cost::orderBy("created_at","desc")->where('name',"like", "%".$this->search."%")->paginate(20);
        }else{
            $costs = Cost::orderBy("created_at","desc")->where('production_id',$this->productionId)->where('name',"like", "%".$this->search."%")->paginate(20);
        }

        return view('livewire.all-costs',
        [
            'costs'=>$costs
        ]);
    }

    public function create(){

        $this->validate([
            'name' => 'required|min:6',
            'value' => 'required|numeric',
        ]);

        $cost= new Cost();
        $cost->name = $this->name;
        $cost->value = $this->value;
        $cost->production_id = $this->productionId;
        $cost->save();

        // notificacion y alerta
        $success_message="nuevo costo añadido";
        
        $users = User::whereHas('roles', function ($query) {
            $query->where('name', 'admin');
        })->get();

        $data = [ 'title' => $success_message,'body' => $cost->name ];
        Notification::send($users, new GenericNotify($data));

        session()->flash('message', $success_message);

        $this->reset('name','value');
    }
}
