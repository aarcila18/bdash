<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Workforce;
use App\Provider;
//notificacion
use App\User;
use App\Notifications\GenericNotify;
use Notification;

class AllWorkforces extends Component
{
    public $search,$name,$cost,$quantity,$provider_id;

    public function render()
    {
        $providers = Provider::orderBy("created_at","desc")->get();
        $workforces = Workforce::orderBy("created_at","desc")->where('name',"like", "%".$this->search."%")->paginate(20);
        return view('livewire.all-workforces',
        [
            'workforces'=>$workforces,
            'providers'=>$providers
        ]);
    }

    public function create(){

        $this->validate([
            'name' => 'required|min:6',
            'cost' => 'required|numeric|min:0|not_in:0',
            'quantity' => 'required|numeric|min:0|not_in:0',
            'provider_id' => 'required|exists:providers,id',
        ]);

        $workforce= new Workforce();
        $workforce->name = $this->name;
        $workforce->cost = $this->cost;
        $workforce->quantity = $this->quantity;
        $workforce->unit_cost = $this->cost/$this->quantity;
        $workforce->provider_id = $this->provider_id;
        $workforce->save();

        // notificacion y alerta
        $success_message="nuevo workforce añadido";
        
        $users = User::whereHas('roles', function ($query) {
            $query->where('name', 'admin');
        })->get();

        $data = [ 'title' => $success_message,'body' => $workforce->name ];
        Notification::send($users, new GenericNotify($data));

        session()->flash('message', $success_message);

        $this->reset();
    }
}
