<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Material;
use App\Product;
use App\MaterialProduct;

class ProductMaterials extends Component
{
    public $product_id,$quantity,$material_id,$total,$selectid,$new_quantity;
    

    public function mount($productid)
    {
        $this->product_id = $productid;
        $product=Product::with('materials')->find($this->product_id);
        $this->total = $product->materials->sum('pivot.material_cost'); 
        $this->selectid = null;
    }

    public function render()
    {
        $materials = Material::all();
        $product=Product::with('materials')->find($this->product_id);
        return view('livewire.product-materials',
        [
            'all_materials'=>$materials,
            'product'=>$product,
        ]);
    }

    public function create(){
        $this->validate([
            'material_id' => 'required',
            'quantity' => 'required',
        ]);

        $product=Product::with('materials')->find($this->product_id);
        $material=Material::find($this->material_id);
        $product->materials()->attach($material, array('material_quantity' => $this->quantity,'material_cost' => $material->unit_cost*$this->quantity, 'quantity'=>$product->stock, 'production_id'=>$product->production_id));

        $this->updateTotals();

        $this->quantity="";
        $this->material_id=0;
        // $this->reset();
    }

    public function updateTotals(){
        $product=Product::with('workforces','materials')->find($this->product_id);
        $production_price = $product->workforces->sum('pivot.workforce_cost')+$product->materials->sum('pivot.material_cost');
        $product->production_price = $production_price;
        $product->save();

        $this->total = $product->materials->sum('pivot.material_cost');
    }

    public function select($selectid){
        $this->selectid = $selectid;
    }

    public function updateMaterial(){
        $product=Product::find($this->product_id);

        $material_pivot = MaterialProduct::where("id",$this->selectid)->first();
        $material=Material::find($material_pivot->material_id);
        $material_pivot->material_quantity = $this->new_quantity;
        $material_pivot->material_cost = $material->unit_cost*$this->new_quantity;
        $material_pivot->quantity = $product->stock;
        $material_pivot->save();

        $this->updateTotals();
        
        $this->selectid = null;
        $this->new_quantity="";
    }

    public function deleteMaterial(){
        $material_pivot = MaterialProduct::where("id",$this->selectid)->first();
        $material_pivot->delete();

        $this->updateTotals();
        
        $this->selectid = null;
        $this->new_quantity="";
    }
}
