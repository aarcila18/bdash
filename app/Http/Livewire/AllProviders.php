<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Provider;
//notificacion
use App\User;
use App\Notifications\GenericNotify;
use Notification;
class AllProviders extends Component
{
    public $search,$name,$cel,$address;

    public function render()
    {
        $providers = Provider::orderBy("created_at","desc")->where('name',"like", "%".$this->search."%")->paginate(20);
        return view('livewire.all-providers',
        [
            'providers'=>$providers
        ]);
    }

    public function create(){

        $this->validate([
            'name' => 'required|min:6',
            'cel' => 'required|min:6',
        ]);

        $provider= new Provider();
        $provider->name = $this->name;
        $provider->cel = $this->cel;
        $provider->address = $this->address;
        $provider->save();

        // notificacion y alerta
        $success_message="nuevo proveedor añadido";
        
        $users = User::whereHas('roles', function ($query) {
            $query->where('name', 'admin');
        })->get();

        $data = [ 'title' => $success_message,'body' => $provider->name ];
        Notification::send($users, new GenericNotify($data));

        session()->flash('message', $success_message);

        $this->reset();
    }
}
