<?php

namespace App\Http\Livewire;

use App\Attachment;
use Livewire\Component;

class AttachmentComponent extends Component
{
    public $attachable_id,$attachable_type;

    public function mount($id,$type)
    {
        $this->attachable_id = $id;
        $this->attachable_type = $type;
    }

    public function render()
    {
        $attachments = Attachment::where("attachable_id","=",$this->attachable_id)->where("attachable_type","=",$this->attachable_type)->get();
        return view('livewire.attachment-component',[
            'attachments' => $attachments
        ]);
    }
}
