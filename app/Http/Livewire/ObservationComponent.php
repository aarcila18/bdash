<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Observations;
//  <livewire:observation-component :id="$campaing->id" :type="get_class($campaing)">
class ObservationComponent extends Component
{
    public $observable_id,$observable_type,$comment;

    public function mount($id,$type)
    {
        $this->observable_id = $id;
        $this->observable_type = $type;
    }

    public function create_comment(){

        $this->validate([
            'comment' => 'required|min:6',
        ]);

        $observation = new Observations();
        $observation->description = $this->comment;
        $observation->observable_id = $this->observable_id;
        $observation->observable_type = $this->observable_type;
        $observation->type = "automatic";
        $observation->user_id = auth()->id();
        $observation->save();

        session()->flash('message', 'Observacion creada');
        // $this->reset();
    }

    public function render()
    {
        $observations = Observations::orderBy("created_at","desc")->with("user")->where("observable_id","=",$this->observable_id)->where("observable_type","=",$this->observable_type)->get();

        return view('livewire.observation-component',["observations"=>$observations]);
    }
}
