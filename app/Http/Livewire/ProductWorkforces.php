<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Workforce;
use App\WorkforceProduct;
use App\Product;
use DB;

class ProductWorkforces extends Component
{
    public $product_id,$quantity,$workforce_id,$total,$selectid,$new_quantity;
    

    public function mount($productid)
    {
        $this->product_id = $productid;
        $product=Product::with('workforces')->find($this->product_id);
        $this->total = $product->workforces->sum('pivot.workforce_cost'); 
        $this->selectid = null;
    }

    public function render()
    {
        $workforces = Workforce::all();
        $product=Product::with('workforces')->find($this->product_id);
        return view('livewire.product-workforces',
        [
            'all_workforces'=>$workforces,
            'product'=>$product,
        ]);
    }

    public function create(){
        $this->validate([
            'workforce_id' => 'required',
            'quantity' => 'required',
        ]);

        $product=Product::with('workforces')->find($this->product_id);
        $workforce=Workforce::find($this->workforce_id);
        $product->workforces()->attach($workforce, array('workforce_quantity' => $this->quantity,'workforce_cost' => $workforce->unit_cost*$this->quantity, 'quantity'=>$product->stock, 'production_id'=>$product->production_id));

        $this->updateTotals();

        $this->quantity="";
        $this->workforce_id=0;
        // $this->reset();
    }

    public function updateTotals(){
        $product=Product::with('workforces','materials')->find($this->product_id);
        $production_price = $product->workforces->sum('pivot.workforce_cost')+$product->materials->sum('pivot.material_cost');
        $product->production_price = $production_price;
        $product->save();

        $this->total = $product->workforces->sum('pivot.workforce_cost');
    }

    public function select($selectid){
        $this->selectid = $selectid;
    }

    public function updateWorkforce(){
        $product=Product::find($this->product_id);

        $workforce_pivot = WorkforceProduct::where("id",$this->selectid)->first();
        $workforce=Workforce::find($workforce_pivot->workforce_id);
        $workforce_pivot->workforce_quantity = $this->new_quantity;
        $workforce_pivot->workforce_cost = $workforce->unit_cost*$this->new_quantity;
        $workforce_pivot->quantity = $product->stock;
        $workforce_pivot->save();

        $this->updateTotals();
        
        $this->selectid = null;
        $this->new_quantity="";
    }

    public function deleteWorkforce(){
        $workforce_pivot = WorkforceProduct::where("id",$this->selectid)->first();
        $workforce_pivot->delete();

        $this->updateTotals();
        
        $this->selectid = null;
        $this->new_quantity="";
    }
}
