<?php

namespace App\Http\Livewire;

use Livewire\Component;

class EditModel extends Component
{
    public $editable_id, $model, $fields, $object,$return_url;

    public function mount($id, $model, $fields,$returnUrl)
    {
        $this->editable_id = $id;
        $this->model = $model;
        $this->fields = $fields;
        $this->return_url = $returnUrl;
    }

    public function render()
    {
        return view('livewire.edit-model');
    }

    public function save()
    {
        foreach ($this->fields as $field) {
            $this->object->{$field['name']} = $field['value'];
        }
        $this->object->save();

        // notificacion y alerta
        $success_message = "Editado con exito !";

        // $users = User::whereHas('roles', function ($query) {
        //     $query->where('name', 'admin');
        // })->get();

        // $data = ['title' => $success_message, 'body' => $production->name];
        // Notification::send($users, new GenericNotify($data));

        session()->flash('message', $success_message);

        return redirect()->to($this->return_url);
    }

    public function query()
    {
        $this->object = $this->model::find($this->editable_id);
        foreach ($this->fields as $key => $field) {
            $this->fields[$key]['value'] = $this->object->{$field['name']};
        }
    }

    public function delete()
    {
        $this->object = $this->model::find($this->editable_id);
        $this->object->delete();
    }
}
