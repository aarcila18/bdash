<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Campaing;
use App\Observations;

class AllCampaings extends Component
{
    public $search,$name,$description;

    public function render()
    {
        $campaings = Campaing::orderBy("created_at","desc")->where('name',"like", "%".$this->search."%")->paginate(20);
        return view('livewire.all-campaings',['campaings' => $campaings]);
    }

    public function create(){
        $campaing= new Campaing();
        $campaing->name = $this->name;
        $campaing->description = $this->description;
        $campaing->save();

        $observation = new Observations();
        $observation->description = "creado con exito";
        $observation->observable_id = $campaing->id;
        $observation->observable_type = get_class($campaing);
        $observation->user_id = auth()->id();
        $campaing->observations()->save($observation);

        $this->reset();

        session()->flash('message', 'Campaña creada');
    }

    
}
