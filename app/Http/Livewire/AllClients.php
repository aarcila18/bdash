<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use App\Observations;
//notificacion
use App\User;
use App\Client;
use App\Role;
use App\Notifications\GenericNotify;
use Notification;

class AllClients extends Component
{
    public $search, $name, $email, $tel;

    public function render()
    {
        $clients = Client::orderBy("created_at", "desc")->where('name', "like", "%" . $this->search . "%")->paginate(20);
        return view(
            'livewire.all-clients',
            [
                'clients' => $clients,
            ]
        );
    }

    public function create()
    {

        $this->validate([
            'name' => 'required|min:6',
            'email' => 'required',
        ]);

        $client = new Client();
        $client->name = $this->name;
        $client->email = $this->email;
        $client->tel = $this->tel;
        $client->save();

        // notificacion y alerta
        $success_message = "nuevo cliente añadido";

        $users = User::whereHas('roles', function ($query) {
            $query->where('name', 'admin');
        })->get();

        $data = ['title' => $success_message, 'body' => $client->name];
        Notification::send($users, new GenericNotify($data));

        session()->flash('message', $success_message);

        $this->reset();
    }

    public function updateClients(){
        try {
            $woocommerce = woo_client();
            $page = 1;
            $count_add = 0;
            $count_update = 0;

            while (count($woocommerce->get('customers',$parameters = [ 'per_page' => 100,'page' => $page ]))>0) {
                $response = $woocommerce->get('customers',$parameters = [ 'per_page' => 100,'page' => $page ]);
                foreach ($response as $client) {
                    $new_client = Client::where('woo_id','=',$client->id)->first();
                    if ($new_client == null) {
                        $new_client = new Client();
                        $new_client->woo_id = $client->id;
                        $new_client->name = $client->first_name." ".$client->last_name;
                        $new_client->email = $client->email;
                        $new_client->tel = "";
                        $new_client->save();
                        $count_add++;

                    }else{
                        $new_client->email = $client->email;
                        $new_client->tel = "";
                        $new_client->save();
                        $count_update++;
                    }
                    
                }
                $page++;
            }
           
            // notificacion y alerta
            $success_message="Clientes creados :".$count_add.", Clientes Actualizados :".$count_update;
            
            $users = User::whereHas('roles', function ($query) {
                $query->where('name', 'admin');
            })->get();

            $data = [ 'title' => "Clientes actualizados",'body' => $success_message ];
            Notification::send($users, new GenericNotify($data));
    
            session()->flash('message', $success_message);

            } catch (\Exception $e) {
                dd($e->getMessage());
            }

    }
}
