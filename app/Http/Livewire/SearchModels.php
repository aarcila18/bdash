<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Product;
use App\Production;
use App\Task;

class SearchModels extends Component
{
    public $search;

    public function render()
    {
        if ($this->search!="" && strlen($this->search)>3) {
            $products = Product::where('type','!=','variation')->where('name',"like", "%".$this->search."%")->get();
            $productions = Production::where('name',"like", "%".$this->search."%")->get();
            $tasks = Task::where('name',"like", "%".$this->search."%")->get();
        }else{
            $products = [];
            $productions = [];
            $tasks = [];
        }
        return view('livewire.search-models',[
            'products'=>$products,
            'productions'=>$productions,
            'tasks'=>$tasks
        ]);
    }
}
