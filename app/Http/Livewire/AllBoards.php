<?php

namespace App\Http\Livewire;

use Livewire\Component;

class AllBoards extends Component
{
    public function render()
    {
        return view('livewire.all-boards');
    }
}
