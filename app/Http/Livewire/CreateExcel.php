<?php

namespace App\Http\Livewire;

use App\Exports\SaleExport;
use Livewire\Component;
use Maatwebsite\Excel\Facades\Excel;

class CreateExcel extends Component
{
    public function render()
    {
        return view('livewire.create-excel');
    }
    public function export() 
    {
        Excel::download(new SaleExport, 'ventas.xlsx');
    }
}
