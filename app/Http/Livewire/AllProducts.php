<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Product;
//notificacion
use App\User;
use App\Notifications\GenericNotify;
use Notification;

class AllProducts extends Component
{

    public $search,$status;

    protected $listeners = ['reloadProducts'];

    public function reloadProducts(){
        $this->reset();
    }

    public function updateProducts(){
        try {    
            $woocommerce = woo_client();
            $page = 1;
            $count_add = 0;
            $count_update = 0;

            while (count($woocommerce->get('products',$parameters = [ 'per_page' => 100,'page' => $page ]))>0) {
                $response = $woocommerce->get('products',$parameters = [ 'per_page' => 100,'page' => $page ]);
                foreach ($response as $product) {
                    $new_product = Product::where('sku','=',$product->sku)->first();
                    if($new_product==null){
                        $new_product = new Product();
                        $new_product->woo_id = $product->id;
                        $new_product->woo_link = $product->permalink;
                        $new_product->sku = $product->sku;
                        $new_product->name = $product->name;
                        // $new_product->description = $product->description;
                        $new_product->type = $product->type;
                        $new_product->image = $product->images[0]->src;
                        $new_product->status = "online";
                        $count_add++;
                    }else{
                        if($new_product->status==="production"){
                        $new_product->woo_id = $product->id;
                        $new_product->woo_link = $product->permalink;
                        $new_product->sku = $product->sku;
                        $new_product->name = $product->name;
                        // $new_product->description = $product->description;
                        $new_product->type = $product->type;
                        $new_product->image = $product->images[0]->src;
                        $new_product->status = "online";
                        $count_update++;
                        }
                    }
                        if ( $product->stock_quantity == null) {
                            $new_product->stock = 0;
                        }else{
                            $new_product->stock = $product->stock_quantity;
                        }
                        $new_product->price = $product->price;
                        $new_product->save();

                        if($product->type=="variable"){
                            $variations = $woocommerce->get('products/'.$product->id.'/variations');
                            $stock_total = 0;
                            foreach ($variations as $variation) {
                                $new_variation = Product::where('sku','=',$variation->id)->first();
                                if($new_variation==null){
                                    $new_variation = new Product();
                                    $new_variation->woo_id = $product->id;
                                    $new_variation->sku = $variation->id.$product->sku;
                                    $new_variation->parent_sku = $product->sku;
                                    $new_variation->name = $product->name." - ".$variation->attributes[0]->option;
                                    // $new_variation->description = $variation->description;
                                    $new_variation->type = "variation";
                                    $new_variation->image = $product->images[0]->src;
                                    $new_variation->status = "online";
                                }
                                if ( $variation->stock_quantity == null) {
                                    $new_variation->stock = 0;
                                }else{
                                    $new_variation->stock = $variation->stock_quantity;
                                    $stock_total = $stock_total + $variation->stock_quantity;
                                }
                                $new_variation->price = $variation->price;
                                $new_variation->save();
                            }
                            $new_product->stock = $stock_total;
                            $new_product->save();
                        }
                     

                    
                }
                $page++;
            }

            // notificacion y alerta
            $success_message="Productos creados :".$count_add.", Productos actualizados :".$count_update;
            
            $users = User::whereHas('roles', function ($query) {
                $query->where('name', 'admin');
            })->get();

            $data = [ 'title' => "Productos actualizados",'body' => $success_message ];
            Notification::send($users, new GenericNotify($data));
           
            session()->flash('message', $success_message);
 
            } catch (\Exception $e) {
                dd($e->getMessage());
            }

    }

    public function render()
    {
        if ($this->status!=null) {
            $status_query = ['status','=',$this->status];
        }else{
            $status_query = ['status','!=',$this->status];
        }

        $products = Product::orderBy("woo_id","desc")->where([
                    ['name',"like", "%".$this->search."%"],
                    ['type','!=','variation'],
                    $status_query
                    ])->orWhere([
                        ['sku',"like", "%".$this->search."%"],
                        ['type','!=','variation'],
                        $status_query
                    ])->paginate(20);
        return view('livewire.all-products',['products' => $products]);
    }
}
