<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Approbation;
//  <livewire:approbation-component :id="$campaing->id" :type="get_class($campaing)">

class ApprobationComponent extends Component
{
    public $approbable_id,$approbable_type,$goal;

    public function mount($id,$type,$goal)
    {
        $this->approbable_id = $id;
        $this->approbable_type = $type;
        $this->goal = $goal;
    }

    public function like(){
        $approbation = new Approbation();
        $approbation->approved = 1;
        $approbation->approbable_id = $this->approbable_id;
        $approbation->approbable_type = $this->approbable_type;
        $approbation->user_id = auth()->id();
        $approbation->save();

        session()->flash('message', 'Has aprobado');

        $this->checkApprobation();
    }

    public function nolike(){
        $approbation = new Approbation();
        $approbation->approved = 0;
        $approbation->approbable_id = $this->approbable_id;
        $approbation->approbable_type = $this->approbable_type;
        $approbation->user_id = auth()->id();
        $approbation->save();

        session()->flash('message', 'Dislike');
    }

    public function checkApprobation(){

        $approbations = Approbation::where("approved",1)->where("approbable_id","=",$this->approbable_id)->where("approbable_type","=",$this->approbable_type)->get();

        if($approbations->count()>=$this->goal){
            $model = $this->approbable_type::find($this->approbable_id);
            $model->approved = 1;
            $model->save();

            session()->flash('message', 'Se ha cumplido el numero de aprobaciones');
        }
    }


    public function render()
    {
        $approbations = Approbation::where("approved",1)->where("approbable_id","=",$this->approbable_id)->where("approbable_type","=",$this->approbable_type)->get();
        $noapprobations = Approbation::where("approved",0)->where("approbable_id","=",$this->approbable_id)->where("approbable_type","=",$this->approbable_type)->get();
        $myapprobations = Approbation::where("approved",1)->where("approbable_id","=",$this->approbable_id)->where("approbable_type","=",$this->approbable_type)->where("user_id",auth()->id())->get();

        return view('livewire.approbation-component',[
            "approbations"=>$approbations,
            "noapprobations"=>$noapprobations,
            "myapprobations"=>$myapprobations
        ]);
    }
}
