<?php

namespace App\Http\Livewire;

use Livewire\Component;

class AllPassives extends Component
{
    public function render()
    {
        return view('livewire.all-passives');
    }
}
