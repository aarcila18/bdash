<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Product;
use App\Observations;
//notificacion
use App\User;
use App\Notifications\GenericNotify;
use Notification;

class CreateProduct extends Component
{
    public $sku,$sku_target,$name,$type,$parent_sku,$stock,$description,$data,$model,$variations;

    public function mount($data,$model){
        $this->data = $data;
        $this->model = $model;
        $this->variations = [["name"=>"","stock"=>0]];
        if ($this->model==="product") {
            $this->type = 'variation';
            $this->parent_sku = $this->data;
            $this->sku = $this->data."-".(Product::latest()->first()->id+1);
            $this->name = Product::where('sku',$this->data)->first()->name." - ";
        }
    }

    public function render()
    {
        return view('livewire.create-product');
    }

    public function create(){

        $this->validate([
            'sku' => 'required|min:6|unique:products',
            'name' => 'required',
            'stock' => 'required',
            'type' => 'required',
        ]);

        $product= new product();
        $product->sku = $this->sku;
        $product->name = $this->name;
        $product->description = $this->description;
        $product->stock = $this->stock;
        $product->type = $this->type;
        $product->parent_sku = $this->parent_sku;
        if($this->model==="production"){
            $product->production_id = $this->data;
        }
        $product->save();

        // notificacion y alerta
        $success_message="nuevo producto añadido";
                
        $users = User::whereHas('roles', function ($query) {
            $query->where('name', 'admin');
        })->get();

        $data = [ 'title' => $success_message,'body' => $product->name ];
        Notification::send($users, new GenericNotify($data));

        session()->flash('message', $success_message);

        $this->emit('reloadProducts');

        // $this->reset();

        if ($this->model==="production") {
            return redirect()->to('/products/'.$product->id);
        }

    }

    public function duplicate(){

        $this->validate([
            'sku' => 'required|min:6|unique:products',
            'sku_target' => 'required|exists:products,sku',
        ]);

        $product= Product::where("sku",$this->sku_target)->first()->load("materials","workforces");
        $product_clone = $product->replicate();
        $product_clone->sku=$this->sku;
        $product_clone->name="copia: ".$product->name;
        $product_clone->status="production";
        if($this->model==="production"){
            $product_clone->production_id = $this->data;
        }
        $product_clone->save();
        foreach($product->materials as $material){
            $product_clone->materials()->attach($material,array('material_quantity' => $material->pivot->material_quantity,'material_cost' => $material->pivot->material_cost,'quantity' => $material->pivot->quantity,'production_id' =>$product_clone->production_id ));
        }
        foreach($product->workforces as $workforce){
            $product_clone->workforces()->attach($workforce,array('workforce_quantity' => $workforce->pivot->workforce_quantity,'workforce_cost' => $workforce->pivot->workforce_cost,'quantity' => $material->pivot->quantity,'production_id' =>$product_clone->production_id ));
        }
        // $product_clone->variations()->attach($product->variations);
        
        // notificacion y alerta
        $success_message="producto copiado";
                
        $users = User::whereHas('roles', function ($query) {
            $query->where('name', 'admin');
        })->get();

        $data = [ 'title' => $success_message,'body' => $product->name ];
        Notification::send($users, new GenericNotify($data));

        session()->flash('message', $success_message);

        $this->emit('reloadProducts');

        // $this->reset();

        if ($this->model==="production") {
            return redirect()->to('/products/'.$product_clone->id);
        }

    }
}
