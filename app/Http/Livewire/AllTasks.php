<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Task;

class AllTasks extends Component
{
    public $title,$board_id,$selected;
    public $description,$status;
    public $todos,$reviews,$doings,$dones;

    public function mount($boardid){
        $this->board_id=$boardid;
        $this->clear();
    }

    public function clear(){
        $this->title="";
        $this->selected=null;
    }

    public function reloadTaks(){
        $this->todos = Task::where("board_id",$this->board_id)->where("status","todo")->get();
        $this->reviews = Task::where("board_id",$this->board_id)->where("status","review")->get();
        $this->doings = Task::where("board_id",$this->board_id)->where("status","doing")->get();
        $this->dones = Task::where("board_id",$this->board_id)->where("status","done")->get();
    }

    public function render()
    {
        $this->reloadTaks();
        return view('livewire.all-tasks');
    }

    public function create($status)
    {
        $task = new Task();
        $task->name = $this->title;
        $task->description = "have a bright day ;)";
        $task->board_id = $this->board_id;
        $task->status = $status;
        $task->save();

        $this->clear();
        
    }

    public function select($id){
        $this->selected=null;
        if ($id!=null) {
            $this->selected=Task::find($id);
            $this->description=$this->selected->description;
            $this->status=$this->selected->status;
        }
    }

    public function edit(){
        $this->selected->description = $this->description;
        $this->selected->status = $this->status;
        $this->selected->save();
    }
}
