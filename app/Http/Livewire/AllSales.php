<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Sale;
use App\ProductSale;
use Carbon\Carbon;
//notificacion
use App\User;
use App\Notifications\GenericNotify;
use Notification;

class AllSales extends Component
{

    public $search;

    public function render()
    {
        $sales = Sale::orderBy("woo_date_created",'desc')->paginate(20);
        return view('livewire.all-sales',['sales' => $sales]);
    }

    public function updateSales(){
        try {
            $woocommerce = woo_client();
            $page = 1;
            $count_add = 0;
            $count_update = 0;

            while (count($woocommerce->get('orders',$parameters = [ 'per_page' => 100,'page' => $page ]))>0) {
                $response = $woocommerce->get('orders',$parameters = [ 'per_page' => 100,'page' => $page ]);
                foreach ($response as $sale) {
                    $new_sale = Sale::where('woo_id','=',$sale->id)->first();
                    if ($new_sale == null) {
                        $new_sale = new Sale();
                        $new_sale->woo_id = $sale->id;
                        $new_sale->order_key = $sale->order_key;
                        $new_sale->discount_total = $sale->discount_total;
                        $new_sale->discount_tax = $sale->discount_tax;
                        $new_sale->shipping_total = $sale->shipping_total;
                        $new_sale->shipping_tax = $sale->shipping_tax;
                        $new_sale->cart_tax = $sale->cart_tax;
                        $new_sale->total = $sale->total;
                        $new_sale->status = $sale->status;
                        $new_sale->woo_date_created = Carbon::parse($sale->date_created);
                        $new_sale->save();
                        $count_add++;

                        foreach ($sale->line_items as $product) {
                            if($product->sku!=null){
                                $new_product_sale = new ProductSale();
                                $new_product_sale->product_sku = $product->sku;
                                $new_product_sale->sale_id = $new_sale->id;
                                $new_product_sale->quantity = $product->quantity;
                                $new_product_sale->save();
                            }
                        }
                    }else{
                        $new_sale->status = $sale->status;
                        $new_sale->save();
                        $count_update++;
                    }
                    
                }
                $page++;
            }
           
            // notificacion y alerta
            $success_message="Ventas creadas :".$count_add.", Ventas Actualizadas :".$count_update;
            
            $users = User::whereHas('roles', function ($query) {
                $query->where('name', 'admin');
            })->get();

            $data = [ 'title' => "Productos actualizados",'body' => $success_message ];
            Notification::send($users, new GenericNotify($data));
    
            session()->flash('message', $success_message);

            } catch (\Exception $e) {
                dd($e->getMessage());
            }

    }
}
