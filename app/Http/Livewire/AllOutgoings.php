<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Outgoing;
//notificacion
use App\User;
use App\Notifications\GenericNotify;
use Notification;

class AllOutgoings extends Component
{
    public $description,$value,$search;

    public function render()
    {
        $outgoings = Outgoing::orderBy("created_at","desc")->where('description',"like","%".$this->search."%")->paginate(20);
        return view('livewire.all-outgoings',['outgoings'=>$outgoings]);
    }

    public function create(){
        
        $this->validate([
            'value' => 'required',
            'description' => 'required|min:6',
        ]);

        $outgoing= new Outgoing();
        $outgoing->value = $this->value;
        $outgoing->description = $this->description;
        $outgoing->save();

        // notificacion y alerta
        $success_message="nuevo gasto añadido";
        
        $users = User::whereHas('roles', function ($query) {
            $query->where('name', 'admin');
        })->get();

        $data = [ 'title' => $success_message,'body' => $outgoing->description ];
        Notification::send($users, new GenericNotify($data));

        session()->flash('message', $success_message);

        $this->reset();
    }
}
