<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Sale;
use App\Outgoing;
use App\Product;
use App\Production;
use App\Provider;
use DB;

class SmartDashNumbers extends Component
{
    public $start_date,$end_date;

    public function mount(){
        $this->end_date=date("Y-m-j");
        $this->start_date=date("Y-m-j",strtotime("-1 months"));
    }

    public function render()
    {
        $sales = Sale::whereBetween('woo_date_created', [$this->start_date." 00:00:00", $this->end_date." 23:59:59"])->where("status","completed")->get();
        $outgoing = Outgoing::whereBetween('created_at', [$this->start_date." 00:00:00", $this->end_date." 23:59:59"])->get();
        $productions = Production::whereBetween('created_at', [$this->start_date." 00:00:00", $this->end_date." 23:59:59"])->get();

        //global
        $products = Product::where('type', '!=', 'variation');
        $total_products = $products->select(DB::raw('sum(stock * price) as total'))->get()->sum('total');
        $total_stock = Product::where('type', '!=', 'variation')->get()->sum('stock');
        $providers = Provider::all();
        
        $numbers = collect([
            ['title'=>'Ventas Periodo','data'=>$sales->count(),'subdata'=>moneyformat($sales->sum('total')),'icon'=>"fa-money-bill-wave"],
            ['title'=>'Gastos Periodo','data'=>$outgoing->count(),'subdata'=>moneyformat($outgoing->sum('value')),'icon'=>"fa-wallet"],
            ['title'=>'Producciones','data'=>$productions->count(),'subdata'=>moneyformat($productions->sum('total')),'icon'=>"fa-industry"],
            ['title'=>'Productos global','data'=>$total_stock,'subdata'=>moneyformat($total_products),'icon'=>"fa-star"],
            ['title'=>'Proovedores global','data'=>$providers->count(),'subdata'=>'','icon'=>"fa-star"],
            ['title'=>'Seguidores Instagram','data'=>2,'subdata'=>2545,'icon'=>"fa-wallet"],
            ['title'=>'Seguidores Facebook','data'=>2,'subdata'=>2545,'icon'=>"fa-wallet"],
            ['title'=>'Activos','data'=>2,'subdata'=>2545,'icon'=>"fa-wallet"],
            ['title'=>'Pasivos','data'=>2,'subdata'=>2545,'icon'=>"fa-wallet"],
            ]);

        return view('livewire.smart-dash-numbers',[
            'numbers'=>$numbers
        ]);
    }
}
