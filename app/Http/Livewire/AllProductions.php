<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Production;
use App\Observations;
//notificacion
use App\User;
use App\Notifications\GenericNotify;
use Notification;

class AllProductions extends Component
{
    public $search, $name, $description, $edit_production;

    public function render()
    {
        $productions = Production::orderBy("created_at", "desc")->where('name', "like", "%" . $this->search . "%")->paginate(20);
        return view(
            'livewire.all-productions',
            [
                'productions' => $productions
            ]
        );
    }

    public function create()
    {

        $this->validate([
            'name' => 'required|min:6',
        ]);

        $production = new Production();
        $production->name = $this->name;
        $production->description = $this->description;
        $production->save();

        // notificacion y alerta
        $success_message = "nueva produccion añadida";

        $users = User::whereHas('roles', function ($query) {
            $query->where('name', 'admin');
        })->get();

        $data = ['title' => $success_message, 'body' => $production->name];
        Notification::send($users, new GenericNotify($data));

        session()->flash('message', $success_message);

        $this->reset();
    }
}
