<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Observations;

// State of results
use App\Sale;
use App\Outgoing;

use App\Product;
use App\ProductSale;

use App\Report;

use DB;
use Carbon\Carbon;

class DashSmartComponent extends Component
{
    public $total_sales,$total_outgoings,$total_actives,$total_passives,$start_date,$end_date;

    public function mount(){
        $this->end_date=date("Y-m-j");
        $this->start_date=date("Y-m-j",strtotime("-1 months"));
        $this->total_sales=0;
        $this->total_outgoings=0;
        $this->total_actives=0;
        $this->total_passives=0;
    }

    public function render()
    {

        //Datos variables
        $observations = Observations::whereBetween('created_at', [$this->start_date." 00:00:00", $this->end_date." 23:59:59"])->paginate(20);
        $sales = Sale::whereBetween('woo_date_created', [$this->start_date." 00:00:00", $this->end_date." 23:59:59"])->where("status","completed")->get();
        $outgoings = Outgoing::whereBetween('created_at', [$this->start_date." 00:00:00", $this->end_date." 23:59:59"])->get();
        $sales_processing = Sale::whereBetween('woo_date_created', [$this->start_date." 00:00:00", $this->end_date." 23:59:59"])->where("status","processing")->get();
        
        //Conteo
        $this->total_sales = $sales->sum('total');
        $total_sales_processing = $sales_processing->sum('total');
        $this->total_outgoings = $outgoings->sum('value');

        // $best_sellers = ProductSale::select(DB::raw('product_sku as product_sku'),DB::raw('sum(quantity) as total'))
        // ->groupby('product_sku')
        // ->orderBy('total','desc')
        // ->paginate(10);

        

        return view('livewire.dash-smart-component',
        [
            'observations'=>$observations,
            'sales'=>$sales,
            'sales_processing'=>$sales_processing,
            'outgoings'=>$outgoings,
            'total_sales_processing'=>$total_sales_processing
            // 'best_sellers'=>$best_sellers,
        ]);
    }

    public function create_report(){
        $report = new Report();
        $report->description = "hola";
        $report->sales = $this->total_sales;
        $report->outgoings = $this->total_outgoings;
        $report->actives = $this->total_actives;
        $report->passives = $this->total_passives;
        $report->save();

        session()->flash('message', 'Reporte creado');
    }
}
