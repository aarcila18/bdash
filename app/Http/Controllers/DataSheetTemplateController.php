<?php

namespace App\Http\Controllers;

use App\DataSheetTemplate;
use Illuminate\Http\Request;

class DataSheetTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DataSheetTemplate  $dataSheetTemplate
     * @return \Illuminate\Http\Response
     */
    public function show(DataSheetTemplate $dataSheetTemplate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DataSheetTemplate  $dataSheetTemplate
     * @return \Illuminate\Http\Response
     */
    public function edit(DataSheetTemplate $dataSheetTemplate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DataSheetTemplate  $dataSheetTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DataSheetTemplate $dataSheetTemplate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DataSheetTemplate  $dataSheetTemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataSheetTemplate $dataSheetTemplate)
    {
        //
    }
}
