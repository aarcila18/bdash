<?php

namespace App\Http\Controllers;

use App\Campaing;
use Illuminate\Http\Request;

class CampaingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view("campaings.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Campaing  $campaing
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $campaing = Campaing::find($id);
        
        return view('campaings.show',['campaing' => $campaing]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Campaing  $campaing
     * @return \Illuminate\Http\Response
     */
    public function edit(Campaing $campaing)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Campaing  $campaing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Campaing $campaing)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Campaing  $campaing
     * @return \Illuminate\Http\Response
     */
    public function destroy(Campaing $campaing)
    {
        //
    }
}
