<?php

namespace App\Http\Controllers;

use App\Production;
use App\Cost;
use App\Exports\ArrayDataExport;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use DB;
use App\Exports\ProductionExport;
use Maatwebsite\Excel\Facades\Excel;

class ProductionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view("productions.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Production  $production
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $production = Production::with("products","productsMaterials",'productsWorkforces')->find($id);
        $production->total_costs = Cost::where("production_id",$production->id)->sum("value");

        // foreach($production->products as $product){
        //     $production_cost+=$product->stock*$product->production_price;
        // }

        $materials = $production->productsMaterials()
        ->select(
            DB::raw('material_id as `material_id`'),
            DB::raw('sum(material_quantity*quantity) as `material_quantity`'),
            DB::raw('sum(material_cost*quantity) as `material_cost`')
            )
        ->groupby('material_id')->get();

        $production->total_materials = $materials->sum("material_cost");

        $workforces = $production->productsWorkforces()
        ->select(
            DB::raw('workforce_id as `workforce_id`'),
            DB::raw('sum(workforce_quantity*quantity) as `workforce_quantity`'),
            DB::raw('sum(workforce_cost*quantity) as `workforce_cost`')
            )
        ->groupby('workforce_id')->get();

        $production->total_workforces = $workforces->sum("workforce_cost");

        $production_cost=$production->total_costs+$production->total_materials+$production->total_workforces;

        $production->total = $production_cost;
        $production->save();
        
        return view('productions.show',
        [
            'production' => $production,
            'production_cost'=>$production_cost,
            'materials'=>$materials,
            'workforces'=>$workforces
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Production  $production
     * @return \Illuminate\Http\Response
     */
    public function edit(Production $production)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Production  $production
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Production $production)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Production  $production
     * @return \Illuminate\Http\Response
     */
    public function destroy(Production $production)
    {
        //
    }

    public function export() 
    {
        return Excel::download(new ProductionExport, 'producciones.xlsx');
    }

    public function exportSingle($id)
    {
        $array = [];

        $production = Production::with("products","productsMaterials",'productsWorkforces',"costs","observations")->find($id);
        $materials = $production->productsMaterials()
        ->select(
            DB::raw('material_id as `material_id`'),
            DB::raw('sum(material_quantity*quantity) as `material_quantity`'),
            DB::raw('sum(material_cost*quantity) as `material_cost`')
            )
        ->groupby('material_id')->get();

        $workforces = $production->productsWorkforces()
        ->select(
            DB::raw('workforce_id as `workforce_id`'),
            DB::raw('sum(workforce_quantity*quantity) as `workforce_quantity`'),
            DB::raw('sum(workforce_cost*quantity) as `workforce_cost`')
            )
        ->groupby('workforce_id')->get();
        $production_cost=$production->total_costs+$production->total_materials+$production->total_workforces;

        array_push($array,['id produccion:',$id]);
        array_push($array,['Cantidad de productos:',$production->products()->count()]);
        array_push($array,['Inventario total:',$production->products()->sum('stock')]);
        array_push($array,['Costo Materiales:',moneyformat($materials->sum('material_cost'))]);
        array_push($array,['Costo Mano de obra:',moneyformat($workforces->sum('workforce_cost'))]);
        array_push($array,['Costos produccion:',moneyformat($production->total_costs)]);
        array_push($array,['Costo total:',moneyformat($production_cost)]);
        array_push($array,['']);
        array_push($array,['sku producto','nombre','costo produccion','inventario','costo inventario']);
        foreach($production->products as $product){
            array_push($array,[$product->sku,$product->name,moneyformat($product->production_price),$product->stock,moneyformat($product->production_price*$product->stock)]); 
        }
        array_push($array,['']);
        array_push($array,['id costo','descripcion','valor']);
        foreach($production->costs as $cost){
            array_push($array,[$cost->id,$cost->name,moneyformat($cost->value)]); 
        }
        array_push($array,['']);
        array_push($array,['id material','cantidad','costo']);
        foreach($materials as $material){
            array_push($array,[$material->material_id,$material->material_quantity,moneyformat($material->material_cost)]); 
        }
        array_push($array,['']);
        array_push($array,['id mano de obra','cantidad','costo']);
        foreach($workforces as $workforce){
            array_push($array,[$workforce->workforce_id,$workforce->workforce_quantity,moneyformat($workforce->workforce_cost)]); 
        }
        array_push($array,['']);
        array_push($array,['Observaciones','Fecha','Mensaje']);
        foreach($production->observations as $observation){
            array_push($array,[$observation->id,$observation->created_at,$observation->description]); 
        }
        $export = new ArrayDataExport($array);

        return Excel::download($export, 'production_'.$id.'_single.xlsx');
    }
}
