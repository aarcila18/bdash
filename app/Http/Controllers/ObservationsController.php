<?php

namespace App\Http\Controllers;

use App\Observations;
use Illuminate\Http\Request;

class ObservationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Observations  $observations
     * @return \Illuminate\Http\Response
     */
    public function show(Observations $observations)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Observations  $observations
     * @return \Illuminate\Http\Response
     */
    public function edit(Observations $observations)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Observations  $observations
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Observations $observations)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Observations  $observations
     * @return \Illuminate\Http\Response
     */
    public function destroy(Observations $observations)
    {
        //
    }
}
