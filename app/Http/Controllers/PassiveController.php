<?php

namespace App\Http\Controllers;

use App\Passive;
use Illuminate\Http\Request;

class PassiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view("passives.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Passive  $passive
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $passive = Passive::find($id);
        
        return view('passives.show',['passive' => $passive]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Passive  $passive
     * @return \Illuminate\Http\Response
     */
    public function edit(Passive $passive)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Passive  $passive
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Passive $passive)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Passive  $passive
     * @return \Illuminate\Http\Response
     */
    public function destroy(Passive $passive)
    {
        //
    }
}
