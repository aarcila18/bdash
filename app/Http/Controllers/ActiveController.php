<?php

namespace App\Http\Controllers;

use App\Active;
use Illuminate\Http\Request;

class ActiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view("actives.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Active  $active
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $active = Active::find($id);
        
        return view('actives.show',['active' => $active]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Active  $active
     * @return \Illuminate\Http\Response
     */
    public function edit(Active $active)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Active  $active
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Active $active)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Active  $active
     * @return \Illuminate\Http\Response
     */
    public function destroy(Active $active)
    {
        //
    }
}
