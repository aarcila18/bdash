<?php

namespace App\Http\Controllers;

use App\Exports\OutgoingExport;
use App\Outgoing;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class OutgoingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view("outgoings.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Outgoing  $outgoing
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $outgoing = Outgoing::find($id);
        
        return view('outgoings.show',['outgoing' => $outgoing]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Outgoing  $outgoing
     * @return \Illuminate\Http\Response
     */
    public function edit(Outgoing $outgoing)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Outgoing  $outgoing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Outgoing $outgoing)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Outgoing  $outgoing
     * @return \Illuminate\Http\Response
     */
    public function destroy(Outgoing $outgoing)
    {
        //
    }
    
    public function export() 
    {
        return Excel::download(new OutgoingExport, 'gastos.xlsx');
    }
}
