<?php

namespace App\Http\Controllers;

use App\Approbation;
use Illuminate\Http\Request;

class ApprobationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Approbation  $approbation
     * @return \Illuminate\Http\Response
     */
    public function show(Approbation $approbation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Approbation  $approbation
     * @return \Illuminate\Http\Response
     */
    public function edit(Approbation $approbation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Approbation  $approbation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Approbation $approbation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Approbation  $approbation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Approbation $approbation)
    {
        //
    }
}
