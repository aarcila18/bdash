<?php

namespace App\Http\Controllers;

use App\SocialTrack;
use Illuminate\Http\Request;

class SocialTrackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SocialTrack  $socialTrack
     * @return \Illuminate\Http\Response
     */
    public function show(SocialTrack $socialTrack)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SocialTrack  $socialTrack
     * @return \Illuminate\Http\Response
     */
    public function edit(SocialTrack $socialTrack)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SocialTrack  $socialTrack
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SocialTrack $socialTrack)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SocialTrack  $socialTrack
     * @return \Illuminate\Http\Response
     */
    public function destroy(SocialTrack $socialTrack)
    {
        //
    }
}
