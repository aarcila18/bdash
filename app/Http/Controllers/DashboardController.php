<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Charts\SalesChart;
use App\Sale;
use App\Outgoing;
// Balance sheet
use App\Passive;
use App\Active;
use App\Product;
use App\ProductSale;
use App\Material;
use App\MaterialProduct;
use App\Workforce;
use App\WorkforceProduct;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;

use DB;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getPercentageChange($oldNumber, $newNumber)
    {
        $decreaseValue = $newNumber - $oldNumber;

        if ($oldNumber == 0) {
            if ($newNumber == 0) {
                return 0;
            }
            return 100;
        }
        if ($newNumber == 0) {
            return 0;
        }
        return ($decreaseValue / $oldNumber) * 100;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //activos - pasivos = patrimonio
        $sales = Sale::whereYear('woo_date_created', now()->year)->whereMonth('woo_date_created', now()->month)->where("status", "completed")->get();
        $outgoings = Outgoing::whereYear('created_at', now()->year)->whereMonth('created_at', now()->month)->get();
        $total_sales = $sales->sum('total');
        $total_outgoings = $outgoings->sum('value');
        //Datos fijos 
        $actives = Active::where('closed', false)->get();
        $passives = Passive::where('paid', false)->get();
        $total_products = Product::where('type', '!=', 'variation')->select(DB::raw('sum(stock * price) as total'))->get()->sum('total');
        $total_stock = Product::where('type', '!=', 'variation')->get()->sum('stock');
        $minimum_stock = Product::where([
            ['type', '!=', 'variation'],
            ['stock', '>', 0],
            ['stock', '<', 5]
        ])->get();

        $total_actives = $actives->sum('value') + $total_products;
        $total_passives = $passives->sum('value');

        $best_sellers = ProductSale::select(DB::raw('product_sku as product_sku'), DB::raw('sum(quantity) as total'))
            ->groupby('product_sku')
            ->orderBy('total', 'desc')
            ->paginate(10);

        // Graficos 
        $months = array(
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July ',
            'August',
            'September',
            'October',
            'November',
            'December',
        );
        $dataMonthsSales = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]];

        for ($i = 0; $i < 3; $i++) {
            $sales_by_month[$i] = Sale::whereYear('woo_date_created', now()->subYear($i)->year)
                ->where([
                    ["status", "completed"],
                ])
                ->select(
                    DB::raw('MONTH(woo_date_created) as `month`'),
                    DB::raw('sum(total) as `total`'),
                    DB::raw('count(*) as `count`')
                )
                ->groupby('month')
                ->orderBy('month', 'asc')
                ->get();
            foreach ($sales_by_month[$i] as $sales) {
                $dataMonthsSales[$i][$sales->month - 1] = $sales->total;
            }
        }

        for ($i = 0; $i < 12; $i++) {
            $media_sales_year[$i] = array_sum($dataMonthsSales[0]) / now()->month;
            $media_by_month[$i] = ($dataMonthsSales[1][$i] + $dataMonthsSales[2][$i]) / 2;
            $goal_sales_year[$i] = $media_sales_year[$i] * 1.20;
            $increment_by_month[0][$i] = $this->getPercentageChange($dataMonthsSales[1][$i], $dataMonthsSales[0][$i]);
            $increment_by_month[1][$i] = $this->getPercentageChange($dataMonthsSales[2][$i], $dataMonthsSales[1][$i]);
        }

        $sales_chart = new SalesChart;
        $sales_chart->minimalist(true);
        $sales_chart->displayLegend(true);
        $sales_chart->title("Ventas ultimos 3 años");
        $sales_chart->labels($months);
        $sales_chart->dataset("Promedio por mes", 'line', $media_by_month)->color('black')->backgroundColor("transparent")->dashed([5]);
        $sales_chart->dataset("Promedio " . now()->subYear(0)->year, 'line', $media_sales_year)->color('blue')->backgroundColor("transparent")->dashed([5]);
        $sales_chart->dataset("Meta " . now()->subYear(0)->year, 'line', $goal_sales_year)->color('green')->backgroundColor("transparent")->dashed([5]);
        $sales_chart->dataset(now()->subYear(0)->year, 'line', $dataMonthsSales[0])->color('#667eea')->backgroundColor("#667eea");
        $sales_chart->dataset(now()->subYear(1)->year, 'line', $dataMonthsSales[1])->color('#cbd5e0')->backgroundColor("#cbd5e0");
        $sales_chart->dataset(now()->subYear(2)->year, 'line', $dataMonthsSales[2])->color('#9ca4ad')->backgroundColor("#9ca4ad");

        // Materials usados 
        
        $materials = Material::count();
        $materials_names = Material::pluck("name")->toArray();
        $materials_array = array_fill(0, $materials, 0);
        $dataMaterials = [$materials_array,$materials_array,$materials_array];

        for ($i = 0; $i < 3; $i++) {

        $materials_by_year[$i] = MaterialProduct::whereYear('created_at', now()->subYear($i)->year)
            ->select(
                DB::raw('material_id as `material_id`'),
                DB::raw('sum(material_quantity*quantity) as `total`')
            )
            ->groupby('material_id')
            ->orderBy('material_id', 'asc')
            ->get();

            foreach ($materials_by_year[$i] as $material) {
                $dataMaterials[$i][$material->material_id - 1] = $material->total;
            }
        }

        $materials_chart = new Chart; // cabmiar a MaterialChart o uno generico
        $materials_chart->displayLegend(true);
        $materials_chart->title("Materiales usados ultimos 3 años");
        $materials_chart->labels($materials_names);
        $materials_chart->dataset(now()->subYear(0)->year,"bar", $dataMaterials[0])->color('#667eea')->backgroundColor("#667eea");
        $materials_chart->dataset(now()->subYear(1)->year,"bar", $dataMaterials[1])->color('#527eea')->backgroundColor("#527eea");
        $materials_chart->dataset(now()->subYear(2)->year,"bar", $dataMaterials[2])->color('#147eea')->backgroundColor("#147eea");

        // workforces usados 
        
        $workforces = Workforce::count();
        $workforces_names = Workforce::pluck("name")->toArray();
        $workforces_array = array_fill(0, $workforces, 0);
        $dataWorkforces = [$workforces_array,$workforces_array,$workforces_array];

        for ($i = 0; $i < 3; $i++) {

        $workforces_by_year[$i] = WorkforceProduct::whereYear('created_at', now()->subYear($i)->year)
            ->select(
                DB::raw('workforce_id as `workforce_id`'),
                DB::raw('sum(workforce_quantity*quantity) as `total`')
            )
            ->groupby('workforce_id')
            ->orderBy('workforce_id', 'asc')
            ->get();

            foreach ($workforces_by_year[$i] as $workforce) {
                $dataWorkforces[$i][$workforce->workforce_id - 1] = $workforce->total;
            }
        }

        $workforces_chart = new Chart; // cabmiar a workforceChart o uno generico
        $workforces_chart->displayLegend(true);
        $workforces_chart->title("workforcees usados ultimos 3 años");
        $workforces_chart->labels($workforces_names);
        $workforces_chart->dataset(now()->subYear(0)->year,"bar", $dataWorkforces[0])->color('#667eea')->backgroundColor("#667eea");
        $workforces_chart->dataset(now()->subYear(1)->year,"bar", $dataWorkforces[1])->color('#527eea')->backgroundColor("#527eea");
        $workforces_chart->dataset(now()->subYear(2)->year,"bar", $dataWorkforces[2])->color('#147eea')->backgroundColor("#147eea");

        // crecimiento 
        $increament_sales_chart = new Chart;
        $increament_sales_chart->title("Ventas incremento");
        $increament_sales_chart->labels($months);
        $increament_sales_chart->dataset(now()->subYear(1)->year . "-" . now()->subYear(0)->year, 'bar', $increment_by_month[0])->color('#667eea')->backgroundColor("#667eea");
        $increament_sales_chart->dataset(now()->subYear(2)->year . "-" . now()->subYear(1)->year, 'bar', $increment_by_month[1])->color('#cbd5e0')->backgroundColor("#cbd5e0");

        // venta media por año

        // crecimiento redes sociales 

        // ventas por categoria

        // torta de gastos vs ventas
        $results_chart = new SalesChart;
        $results_chart->minimalist(true);
        $results_chart->displayLegend(true);
        $results_chart->title("Ventas vs Gastos ultimo mes");
        $results_chart->labels(["Ventas", "Gastos"]);
        $results_chart->dataset("pie", 'pie', [$total_sales, $total_outgoings])->backgroundColor(["#667eea", "#9ca4ad"]);

        return view('dashboard', [
            'sales_chart' => $sales_chart,
            'materials_chart'=>$materials_chart,
            'workforces_chart'=>$workforces_chart,
            'results_chart' => $results_chart,
            'increament_sales_chart' => $increament_sales_chart,
            'actives' => $actives,
            'passives' => $passives,
            'total_products' => $total_products,
            'total_stock' => $total_stock,
            'minimum_stock' => $minimum_stock,
            'total_actives' => $total_actives,
            'total_passives' => $total_passives,
            'best_sellers' => $best_sellers,
        ]);
    }
}
