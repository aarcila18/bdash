<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    //
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }

    public function observations()
    {
        return $this->morphMany('App\Observations', 'observable');
    }

}
