<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialTrack extends Model
{
    //
    public function campaing()
    {
        return $this->belongsTo(Campaing::class);
    }

    public function observations()
    {
        return $this->morphMany('App\Observations', 'observable');
    }
}
