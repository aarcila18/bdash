<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    //
    public function materials()
    {
        return $this->hasMany(Material::class);
    }

    public function worforces()
    {
        return $this->hasMany(Worforce::class);
    }

    public function observations()
    {
        return $this->morphMany('App\Observations', 'observable');
    }
    
    public function attachments()
    {
        return $this->morphMany('App\Attachment', 'attachable');
    }
}
