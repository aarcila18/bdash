<?php
use Automattic\WooCommerce\Client;
use App\Config;

function woo_client(){
    $config = Config::find(1);
    $woocommerce = new Client(
        $config->woo_url, 
        $config->woo_key, 
        $config->woo_secret,
        [
            'version' => 'wc/v3',
        ]
    );

    return $woocommerce;
}