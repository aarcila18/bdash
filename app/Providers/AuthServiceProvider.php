<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //registrar los gates para usarlos como middlewares
        Gate::define('accessAdmin',function($user){
            return $user->hasRole("admin");
        });
        Gate::define('accessData',function($user){
            return $user->hasAnyRole(["admin","assistant","user"]);
        });
        Gate::define('userEdit',function($user){
            return $user->hasAnyRole(["admin","assistant","editor"]);
        });
        Gate::define('userDelete',function($user){
            return $user->hasAnyRole(["admin","assistant"]);
        });
        Gate::define('approve',function($user){
            return $user->hasAnyRole(["admin","assistant"]);
        });
        Gate::define('accessCompany',function($user){
            return $user->hasAnyRole(["admin","assistant","editor","user"]);
        });
    }
}
