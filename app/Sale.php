<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    //
    public function campaing()
    {
        return $this->belongsTo(Campaing::class);
    }

    public function observations()
    {
        return $this->morphMany('App\Observations', 'observable');
    }

    public function products(){
        return $this->belongsToMany(
            Product::class,
            "products_sales",
            'sale_id',
            'product_sku',
            'id',
            'sku')->withPivot('quantity');
    }

    public function attachments()
    {
        return $this->morphMany('App\Attachment', 'attachable');
    }
}
