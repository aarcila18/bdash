<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialProduct extends Model
{
    //
    protected $table = 'material_product';

    public function production()
    {
        return $this->belongsTo(Production::class);
    }
}
