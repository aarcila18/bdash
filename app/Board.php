<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    //
    public function observations()
    {
        return $this->morphMany('App\Observations', 'observable');
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function attachments()
    {
        return $this->morphMany('App\Attachment', 'attachable');
    }
}
