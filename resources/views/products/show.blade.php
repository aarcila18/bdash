@extends('layouts.app',['title' => 'producto'])

@section('content')
<div class="container p-2 mt-5 mx-auto">
    <div class="row flex">
        <a href="{{ URL::previous() }}" class="bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-2 my-4 rounded-full cursor-pointer">
            <i  class="fas fa-chevron-circle-left fa-fw mr-3 ml-3"></i>
        </a>
        <livewire:edit-model :returnUrl="URL::current()" :id="$product->id" :model="get_class($product)" :fields="[
            ['type'=>'text','lable'=>'Nombre','name'=>'name','value'=>''],
            ['type'=>'number','lable'=>'Inventario','name'=>'stock','value'=>''],
            ['type'=>'number','lable'=>'Precio','name'=>'price','value'=>'']
            ]">
        <div class="row py-5 text-right">
            <a class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded" href="" target="_blank">Exportar</a>
        </div>
    </div>

            <h1 class="relative my-5 p-3 uppercase text-lg nunito font-bold">{{$product->name}}</h1>
            <h2>Stock: {{$product->stock}}</h2>
            <p>{{$product->description}}</p>
            <img class="w-64" src="{{$product->image}}" alt="">
            @if($product->woo_link!=null)
            <a href="{{$product->woo_link}}">Ver producto en la web</a>
            @endif
            @if($product->type==="variable" && $product->status==="production")
            <livewire:create-product :data="$product->sku" :model="'product'">
            @endif
            @if($product->type==="variable")
            <h2 class="text-sm mb-5 font-bold">Variaciones:</h2>
            <table class="table-auto w-full">
                <tbody>
                    @foreach($product->variations as $variation)
                    <tr>
                        <td class="border px-4 py-2">{{$variation->sku}}</td>
                        <td class="border px-4 py-2">{{$variation->type}}</td>
                        <td class="border px-4 py-2">{{$variation->name}}</td>
                        <td class="border px-4 py-2">{{$variation->stock}}</td>
                        <td class="border px-4 py-2 flex">
                            <a href="{{ route('products.show', $variation->id) }}" class="block py-1 md:py-3 pl-1 align-middle text-gray-600 no-underline hover:text-indigo-400">
                                <i class="fas fa-eye fa-fw mr-3"></i>
                            </a>
                            @can("userEdit")
                            <a href="" class="block py-1 md:py-3 pl-1 align-middle text-gray-600 no-underline hover:text-indigo-400">
                                <i class="fas fa-edit fa-fw mr-3"></i>
                            </a>
                            @endcan
                            @can("userDelete")
                            <a href="#" onclick="return confirm('¿ Estas seguro ?')" class="block py-1 md:py-3 pl-1 align-middle text-gray-600 no-underline hover:text-indigo-400">
                                <i class="fas fa-trash fa-fw mr-3"></i>
                            </a>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
            <div>
                <h2>Datos de produccion</h2>
                <div class="my-5" x-data="{ tab: 'costos' }">
                    <button :class="{ 'bg-indigo-400': tab === 'costos' }" class="bg-gray-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded" @click="tab = 'costos'">costos</button>
                    <button :class="{ 'bg-indigo-400': tab === 'materiales' }" class="bg-gray-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded" @click="tab = 'materiales'">materiales</button>
                    <button :class="{ 'bg-indigo-400': tab === 'manoobra' }" class="bg-gray-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded" @click="tab = 'manoobra'">mano de obra</button>

                    <div class="p-5" x-show="tab === 'costos'">
                        <p>Precio de venta: {{moneyformat($product->price)}}</p>
                        <p>Costos de produccion: {{moneyformat($product->production_price)}}</p>
                        <p>Costos de produccion stock: {{moneyformat($product->production_price*$product->stock)}}</p>
                        @if($product->price!=null)
                        <p>Margen de contribucion: {{(($product->price-$product->production_price)/$product->price)*100}}%</p>
                        @endif
                    </div>
                    <div class="p-5" x-show="tab === 'materiales'">
                        <livewire:product-materials :productid="$product->id">
                    </div>
                    <div class="p-5" x-show="tab === 'manoobra'">
                        <livewire:product-workforces :productid="$product->id">
                    </div>
                </div>


            </div>

            <livewire:observation-component :id="$product->id" :type="get_class($product)">
</div>
@endsection