@extends('layouts.app',['title' => 'productos'])

@section('content')
<div class="container p-2 mt-5 mx-auto">
    @livewire('all-products')
</div>
@endsection
