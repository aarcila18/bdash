@extends('layouts.app')

@section('content')
<div class="container p-2 mt-5">
    @livewire('all-products')
</div>
@endsection
