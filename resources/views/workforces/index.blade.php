@extends('layouts.app',['title' => 'Mano de obra'])

@section('content')
<div class="container p-2 mt-5 mx-auto">
    @livewire('all-workforces')
</div>
@endsection
