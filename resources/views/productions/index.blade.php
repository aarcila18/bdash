@extends('layouts.app',['title' => 'producciones'])

@section('content')
<div class="container p-2 mt-5 mx-auto">
    @livewire('all-productions')
</div>
@endsection
