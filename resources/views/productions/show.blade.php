@extends('layouts.app',['title'=>$production->name])

@section('content')
<div class="container p-2 mt-5 mx-auto">
    <div class="row flex">
        <a href="{{ URL::previous() }}" class="bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-2 my-4 rounded-full cursor-pointer">
            <i  class="fas fa-chevron-circle-left fa-fw mr-3 ml-3"></i>
        </a>
        <livewire:edit-model :returnUrl="URL::current()" :id="$production->id" :model="get_class($production)" :fields="[
        ['type'=>'text','lable'=>'Nombre','name'=>'name','value'=>''],
        ['type'=>'text','lable'=>'Descripcion','name'=>'description','value'=>''],
        ['type'=>'date','lable'=>'Vencimiento','name'=>'limit_at','value'=>''],
        ['type'=>'number','lable'=>'Presupuesto','name'=>'budget','value'=>'']
        ]">
        <div class="row py-5 text-right">
            <a class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded" href="{{ route('productions.single.export', $production->id) }}" target="_blank">Exportar</a>
        </div>
    </div>



    <livewire:approbation-component :id="$production->id" :type="get_class($production)" :goal="3">

    <h1 class="relative my-5 p-3 uppercase text-lg nunito font-bold">{{$production->name}}</h1>
    <p>{{$production->description}}</p>
    <h2 class="relative my-5 p-3 uppercase text-md nunito font-bold">Resumen:</h2>

    @php($products = $production->products)

    <p> Cantidad de productos : {{$products->count()}}  </p>
    <p> Inventario total: {{$products->sum('stock')}}  </p>
    <p> Costo Materiales : {{moneyformat($materials->sum('material_cost'))}} </p>
    <p> Costo Mano de obra : {{moneyformat($workforces->sum('workforce_cost'))}} </p>
    <p> Costos produccion : {{moneyformat($production->total_costs)}} </p>
    <p> Costo total : {{moneyformat($production_cost)}} </p>
    
    <h2 class="relative my-5 p-3 uppercase text-md nunito font-bold">Productos:</h2>
    <livewire:create-product :data="$production->id" :model="'production'">
    @if(count($production->products)==0)
        Esta Produccion No tiene productos asignados
    @else
        @include("components.products-table",['page' => 'productions'])
    @endif

    <h2 class="relative my-5 p-3 uppercase text-md nunito font-bold">Costos de produccion:</h2>
    <livewire:all-costs :productionId="$production->id">

    <h2 class="relative my-5 p-3 uppercase text-md nunito font-bold">Materiales usados:</h2>
    <table class="table-auto w-full">
    <thead>
        <tr>
        <th class="px-4 py-2">ID</th>
        <th class="px-4 py-2">QUANTITY</th>
        <th class="px-4 py-2">COSTS</th>
        </tr>
    </thead>
    <tbody>
    @foreach($materials as $material)
        <tr>
            <td class="border px-4 py-2">{{$material->id}}</td>
            <td class="border px-4 py-2">{{$material->material_quantity}}</td>
            <td class="border px-4 py-2">{{moneyformat($material->material_cost)}}</td>
        </tr>          
    @endforeach
        
    </tbody>
    </table>

    <h2 class="relative my-5 p-3 uppercase text-md nunito font-bold">Mano de obra usada:</h2>
    <table class="table-auto w-full">
    <thead>
        <tr>
        <th class="px-4 py-2">ID</th>
        <th class="px-4 py-2">QUANTITY</th>
        <th class="px-4 py-2">COSTS</th>
        </tr>
    </thead>
    <tbody>
    @foreach($workforces as $workforce)
        <tr>
            <td class="border px-4 py-2">{{$workforce->id}}</td>
            <td class="border px-4 py-2">{{$workforce->workforce_quantity}}</td>
            <td class="border px-4 py-2">{{moneyformat($workforce->workforce_cost)}}</td>
        </tr>          
    @endforeach
        
    </tbody>
    </table>

    <livewire:observation-component :id="$production->id" :type="get_class($production)">

</div>
@endsection