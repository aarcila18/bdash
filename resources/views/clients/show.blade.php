@extends('layouts.app')

@section('content')
<div class="container p-2 mt-5 mx-auto">
    <div class="row my-5">
        <a href="{{ URL::previous() }}" class="bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-2 my-5 rounded-full cursor-pointer">
            <i  class="fas fa-chevron-circle-left fa-fw mr-3 ml-3"></i>
        </a>
    </div>
    <livewire:edit-model :returnUrl="URL::current()" :id="$client->id" :model="get_class($client)" :fields="[
                        ['type'=>'text','lable'=>'Nombre','name'=>'name','value'=>''],
                        ]">
    <h1>{{$client->name}}</h1>
    <livewire:observation-component :id="$client->id" :type="get_class($client)">
</div>
@endsection