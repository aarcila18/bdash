@extends('layouts.app',['title' => 'Clientes'])

@section('content')
<div class="container p-2 mt-5 mx-auto">
    @livewire('all-clients')
</div>
@endsection
