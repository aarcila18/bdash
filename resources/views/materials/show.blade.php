@extends('layouts.app')

@section('content')
<div class="container p-2 mt-5 mx-auto">
    <div class="row my-5">
        <a href="{{ URL::previous() }}" class="bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-2 my-5 rounded-full cursor-pointer">
            <i  class="fas fa-chevron-circle-left fa-fw mr-3 ml-3"></i>
        </a>
    </div>
    <livewire:edit-model :returnUrl="URL::current()" :id="$material->id" :model="get_class($material)" :fields="[
                        ['type'=>'text','lable'=>'Nombre','name'=>'name','value'=>''],
                        ['type'=>'number','lable'=>'Costo','name'=>'cost','value'=>''],
                        ['type'=>'number','lable'=>'Cantidad','name'=>'quantity','value'=>''],
                        ['type'=>'number','lable'=>'Costo unidad','name'=>'unit_cost','value'=>'']
                        ]">
    <h1>{{$material->name}}</h1>
    <livewire:observation-component :id="$material->id" :type="get_class($material)">
</div>
@endsection