@extends('layouts.app',['title' => 'materiales'])

@section('content')
<div class="container p-2 mt-5 mx-auto">
    @livewire('all-materials')
</div>
@endsection
