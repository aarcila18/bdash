@extends('layouts.app',['title' => 'notificaciones'])

@section('content')
<div class="container p-2 mt-5 mx-auto">
    @livewire('all-notifications')
</div>
@endsection
