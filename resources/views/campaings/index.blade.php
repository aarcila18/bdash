@extends('layouts.app',['title' => 'campañas'])

@section('content')
<div class="container p-2 mt-5 mx-auto">
    @livewire('all-campaings')
</div>
@endsection
