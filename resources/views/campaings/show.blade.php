@extends('layouts.app',['title' => $campaing->name])

@section('content')
<div class="container p-2 mt-5">
    <div class="row my-5">
        <a href="{{ URL::previous() }}" class="bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-2 my-5 rounded-full cursor-pointer">
            <i  class="fas fa-chevron-circle-left fa-fw mr-3 ml-3"></i>
        </a>
    </div>
    <livewire:edit-model :returnUrl="URL::current()" :id="$campaing->id" :model="get_class($campaing)" :fields="[
                        ['type'=>'text','lable'=>'Nombre','name'=>'name','value'=>''],
                        ]">
    <livewire:observation-component :id="$campaing->id" :type="get_class($campaing)">
</div>
@endsection