@extends('layouts.app',['title' => 'dash'])

@section('content')  
<style>
    canvas{
        max-height:450px !important;
    }
</style>  
<div class="container flex flex-row flex-wrap flex-1 flex-grow content-start ">
    <!--Dash Content -->
    <div id="dash-content" class="bg-gray-200 lg:py-0 w-full lg:max-w-sm flex flex-wrap content-start">
        @livewire('smart-dash-numbers')
    </div>

    <!--Graph Content -->
    <div id="main-content" class="w-full flex-1 p-10"> 
        <h2>Global: </h2>
        <div id="charts" class="my-10">
            <div class="my-5 border-2 border-gray-400 border-dashed bg-gray-200">
                {!! $sales_chart->container() !!}
            </div>
            <div class="my-5 border-2 border-gray-400 border-dashed bg-gray-200">
                {!! $materials_chart->container() !!}
            </div>
            <div class="my-5 border-2 border-gray-400 border-dashed bg-gray-200">
                {!! $workforces_chart->container() !!}
            </div>
            <div class="my-5 border-2 border-gray-400 border-dashed bg-gray-200">
                {!! $increament_sales_chart->container() !!}
            </div>
            <div class="my-5 border-2 border-gray-400 border-dashed bg-gray-200">
                {!! $results_chart->container() !!}
            </div>
            
        </div>  
        <div id="results">
            <h2>Estado de resultados periodo</h2>
            @livewire('dash-smart-component')
        </div>
        <div id="general_balance">
            <h2>Balance general</h2>
            <div class="my-5 p-8 border-2 border-gray-400 border-dashed bg-gray-200">
                <h3>{{count($actives)}} Activos</h3>
                <table class="table-auto w-full">
                    <thead>
                        <tr>
                            <th class="px-4 py-2">DATE</th>
                            <th class="px-4 py-2">DESCRIPTION</th>
                            <th class="px-4 py-2">VALUE</th>
                            <th class="px-4 py-2">TYPE</th>
                            <th class="px-4 py-2"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($actives as $active)
                            <tr>
                                <td class="border px-4 py-2">{{$active->created_at}}</td>
                                <td class="border px-4 py-2">{{$active->description}}</td>
                                <td class="border px-4 py-2">{{moneyformat($active->value)}}</td>
                                <td class="border px-4 py-2">{{$active->type}}</td>
                                <td class="border px-4 py-2"><a href="">show</a></td>
                            </tr>
                        @endforeach
                        <tr>
                        <td class="border px-4 py-2">GLOBAL</td>
                        <td class="border px-4 py-2">Stock total: {{$total_stock}}</td>
                        <td class="border px-4 py-2"><h2>{{moneyformat($total_products)}}</h2></td>
                        <td class="border px-4 py-2"></td>
                        <td class="border px-4 py-2"></td>
                        </tr>
                        <tr>
                        <td></td>
                        <td class="border px-4 py-2">total</td>
                        <td class="border px-4 py-2"><h2>{{moneyformat($total_actives)}}</h2></td>
                        <td></td>
                        </tr>
                    </tbody>
                </table>   
            </div>
            <div class="my-5 p-8 border-2 border-gray-400 border-dashed bg-gray-200">
                <h3>{{count($passives)}} Pasivos</h3>
                <table class="table-auto w-full">
                    <thead>
                        <tr>
                        <th class="px-4 py-2">DATE</th>
                        <th class="px-4 py-2">DESCRIPTION</th>
                        <th class="px-4 py-2">VALUE</th>
                        <th class="px-4 py-2">TYPE</th>
                        <th class="px-4 py-2"></th>
                        </tr>
                    </thead>
                    <tbody>
                    
                        @foreach($passives as $passive)
                            <tr>
                            <td class="border px-4 py-2">{{$passive->created_at}}</td>
                            <td class="border px-4 py-2">{{$passive->description}}</td>
                            <td class="border px-4 py-2">{{moneyformat($passive->value)}}</td>
                            <td class="border px-4 py-2">{{$passive->type}}</td>
                            <td class="border px-4 py-2"><a href="">show</a></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td></td>
                            <td class="border px-4 py-2">total</td>
                            <td class="border px-4 py-2"><h2>{{moneyformat($total_passives)}}</h2></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>     
            </div>
        </div> 
        <div id="varius_data">
            <h3>PRODUCTOS CON POCO STOCK:</h3>
            <div class="my-5 p-8 border-2 border-gray-400 border-dashed bg-gray-200">
                <table class="table-auto w-full">
                    <thead>
                        <tr>
                            <th class="px-4 py-2">SKU</th>
                            <th class="px-4 py-2">NOMBRE</th>
                            <th class="px-4 py-2">STOCK</th>
                            <th class="px-4 py-2">PRICE</th>
                            <th class="px-4 py-2">STOCK VALUE</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                        @foreach($minimum_stock as $product)
                            @if($product->parent_sku==null)
                            <tr>
                                <td class="border px-4 py-2">{{$product->sku}}</td>
                                <td class="border px-4 py-2">{{$product->name}}</td>
                                <td class="border px-4 py-2">{{$product->stock}}</td>
                                <td class="border px-4 py-2">{{moneyformat($product->price)}}</td>
                                <td class="border px-4 py-2">{{moneyformat($product->stock*$product->price)}}</td>
                                <td class="border px-4 py-2"><a href="{{ route('products.show', $product->id) }}">show</a></td>
                            </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
            <h3>PRODUCTOS mas vendidos:</h3>
            <div class="my-5 p-8 border-2 border-gray-400 border-dashed bg-gray-200">
                <table class="table-auto w-full">
                    <thead>
                        <tr>
                        <th class="px-4 py-2">SKU</th>
                        <th class="px-4 py-2">total</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                        @foreach($best_sellers as $product)
                            @if($product->parent_sku==null)
                            <tr>
                                <td class="border px-4 py-2">{{$product->product_sku}}</td>
                                <td class="border px-4 py-2">{{$product->total}}</td>
                            </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>    
        </div>
    </div>
</div>

@endsection

@section('scripts')   
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
{!! $sales_chart->script() !!}
{!! $materials_chart->script() !!}
{!! $workforces_chart->script() !!}
{!! $results_chart->script() !!}
{!! $increament_sales_chart->script() !!}

@endsection