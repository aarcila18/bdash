@extends('layouts.app',['title' => 'pasivos'])

@section('content')
<div class="container p-2 mt-5 mx-auto">
    @livewire('all-passives')
</div>
@endsection
