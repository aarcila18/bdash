<div>
    <table class="table-auto w-full">
        <thead>
            <tr>
            <th class="px-4 py-2">SKU</th>
            <th class="px-4 py-2">TYPE</th>
            <th class="px-4 py-2">STATUS</th>
            <th class="px-4 py-2">NOMBRE</th>
            <th class="px-4 py-2">STOCK</th>
            <th class="px-4 py-2">PRICE</th>
            <th class="px-4 py-2">STOCK VALUE</th>
            </tr>
        </thead>
        <tbody>
        
            @foreach($products as $product)
                @if($product->parent_sku==null)
                <tr>
                    <td class="border px-4 py-2">{{$product->sku}}</td>
                    <td class="border px-4 py-2">{{$product->type}}</td>
                    <td class="border px-4 py-2">{{$product->status}}</td>
                    <td class="border px-4 py-2">{{$product->name}}</td>
                    <td class="border px-4 py-2">{{$product->stock}}</td>
                    @if($page==="products")
                    <td class="border px-4 py-2">{{moneyformat($product->price)}}</td>
                    <td class="border px-4 py-2">{{moneyformat($product->stock*$product->price)}}</td>
                    @else
                    <td class="border px-4 py-2">{{moneyformat($product->production_price)}}</td>
                    <td class="border px-4 py-2">{{moneyformat($product->stock*$product->production_price)}}</td>
                    @endif
                    <td class="border px-4 py-2 flex">
                        <a href="{{ route('products.show', $product->id) }}" class="block py-1 md:py-3 pl-1 align-middle text-gray-600 no-underline hover:text-indigo-400">
                            <i class="fas fa-eye fa-fw mr-3"></i>
                        </a>

                </tr>
                @endif
            @endforeach
        </tbody>
    </table>
</div>