@if (session()->has('message'))
<div class="row">
    <div class="flex bg-green-lightest p-4">
        <div class="mr-4">
            <div class="h-10 w-10 text-white bg-green-500 rounded-full flex justify-center items-center">
                <i class="fas fa-check fa-fw mr-3"></i>
            </div>
        </div>
        <div class="flex justify-between w-full">
            <div class="text-green-600">
                <p class="mb-2 font-bold">
                ¡ Bien hecho !
                </p>
                <p class="text-xs">
                {{ session('message') }}
                </p>
            </div>
            <div class="text-sm text-grey">
                <span>x</span>
            </div>
        </div>
    </div>
</div>
@endif
