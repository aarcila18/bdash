<div x-data="{ open: false,tab: 'create' }" class="my-10">
    <button @click="open = true" x-show="!open" class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded">+ Añadir @if($type=="variation") variacion @else producto @endif</button>
    <div class="p-10 bg-gray-300 rounded-lg" x-show.transition.duration.300ms="open" @click.away="open = false">
        <h2 class="nunito text-md text-bold mb-5">Crear @if($type=="variation") variacion @else producto @endif</h2>
        <div class="my-5">
            <a @click="tab = 'create'">Nuevo</a>/
            <a @click="tab = 'duplicate'">Duplicar</a>
        </div>
        <div x-show.transition.duration.300ms="tab === 'create'">
            <input wire:model="sku" class="bg-gray-200 mb-5 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Sku">
            @error('sku') <span class="error">{{ $message }}</span> @enderror
            @if($type!="variation")
            <select wire:model="type" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                    <option value="" disable>Tipo</option>
                    <option>simple</option>
                    <option>variable</option>
            </select>
            @error('type') <span class="error">{{ $message }}</span> @enderror
            @endif
            <input wire:model="name" class="bg-gray-200 mb-5 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Nombre Producto">
            @error('name') <span class="error">{{ $message }}</span> @enderror
            <input wire:model="description" class="bg-gray-200 mb-5 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Descripcion corta">
            <input wire:model="stock" class="bg-gray-200 mb-5 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Stock">
            @error('stock') <span class="error">{{ $message }}</span> @enderror
            <button wire:click="create" class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded">
                Crear
            </button>
            <div class="row pt-5">
                <span class="text-sm text-gray-600">* crea un producto varible para añadirle variaciones</span>
            </div>
        </div>
        <div x-show.transition.duration.300ms="tab === 'duplicate'">
            <input wire:model="sku" class="bg-gray-200 mb-5 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Sku">
            @error('sku') <span class="error">{{ $message }}</span> @enderror
            <input wire:model="sku_target" class="bg-gray-200 mb-5 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Sku a copiar">
            @error('sku_target') <span class="error">{{ $message }}</span> @enderror
            <button wire:click="duplicate" class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded">
                Crear
            </button>
        </div>
    </div>
</div>