<div class="w-1/2 lg:w-full">
    <div class="border-2 border-gray-400 border-dashed hover:border-transparent hover:bg-white hover:shadow-xl rounded p-6 m-2 md:mx-10 md:my-6">
        <div class="flex flex-col items-center">
            <div class="flex-shrink">
                <div class="rounded-full p-3"><i style="font-size: 35px;" class="fa {{$icon}} fa-fw fa-inverse text-indigo-500"></i></div>
            </div>
            <div class="flex-1">
                <h3 class="font-bold text-center text-3xl">{{$number}}</h3>
                <p class="font-bold text-center text-l">{{$subdata}}</p>
                <h5 class="font-bold text-center text-gray-500">{{$title}}</h5>
            </div>
        </div>
    </div>
</div>