@extends('layouts.app',['title' => 'Usuarios'])

@section('content')
<div class="container p-2 mt-5 mx-auto">
    @livewire('all-users')
</div>
@endsection
