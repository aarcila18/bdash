@extends('layouts.app',['title' => 'proveedores'])

@section('content')
<div class="container p-2 mt-5 mx-auto">
    @livewire('all-providers')
</div>
@endsection
