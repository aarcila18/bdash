@extends('layouts.app')

@section('content')
<div class="container p-2 mt-5 mx-auto">
    <div class="row my-5">
        <a href="{{ URL::previous() }}" class="bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-2 my-5 rounded-full cursor-pointer">
            <i  class="fas fa-chevron-circle-left fa-fw mr-3 ml-3"></i>
        </a>
    </div>
    <livewire:edit-model :returnUrl="URL::current()" :id="$outgoing->id" :model="get_class($outgoing)" :fields="[
                        ['type'=>'text','lable'=>'Descripcion','name'=>'description','value'=>''],
                        ['type'=>'number','lable'=>'Valor','name'=>'value','value'=>''],
                        ['type'=>'date','lable'=>'Fecha creacion','name'=>'created_at','value'=>'']
                        ]">
                        
    <livewire:observation-component :id="$outgoing->id" :type="get_class($outgoing)">
</div>
@endsection