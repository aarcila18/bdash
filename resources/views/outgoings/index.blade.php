@extends('layouts.app',['title' => 'gastos'])

@section('content')
<div class="container p-2 mt-5 mx-auto">
    @livewire('all-outgoings')
</div>
@endsection
