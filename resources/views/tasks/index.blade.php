@extends('layouts.app',['title' => 'Tareas'])

@section('content')
<div class="container p-2 mt-5 mx-auto">
    <livewire:all-boards>
    <livewire:all-tasks :boardid="1">
    <livewire:all-tasks :boardid="2">
</div>
@endsection
