@extends('layouts.app')

@section('content')
<div class="container p-2 mt-5 mx-auto">
    <div class="row my-5">
        <a href="{{ URL::previous() }}" class="bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-2 my-5 rounded-full cursor-pointer">
            <i  class="fas fa-chevron-circle-left fa-fw mr-3 ml-3"></i>
        </a>
    </div>
    <livewire:edit-model :returnUrl="URL::current()" :id="$sale->id" :model="get_class($sale)" :fields="[
                        ['type'=>'text','lable'=>'Nombre','name'=>'woo_id','value'=>''],
                        ['type'=>'number','lable'=>'Total','name'=>'total','value'=>'']
                        ]">
    <table class="table-auto w-full">
        <thead>
            <tr>
            <th class="px-4 py-2">SKU</th>
            <th class="px-4 py-2">TYPE</th>
            <th class="px-4 py-2">NOMBRE</th>
            <th class="px-4 py-2">PRICE</th>
            </tr>
        </thead>
        <tbody>
        @foreach($sale->products as $product)
            <tr>
                <td class="border px-4 py-2">{{$product->sku}}</td>
                <td class="border px-4 py-2">{{$product->type}}</td>
                <td class="border px-4 py-2">{{$product->name}} * {{$product->pivot->quantity}}</td>
                <td class="border px-4 py-2">{{moneyformat($product->price*$product->pivot->quantity)}}</td>
                <td class="border px-4 py-2"><a href="{{ route('products.show', $product->id) }}">show</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <livewire:observation-component :id="$sale->id" :type="get_class($sale)">
</div>
@endsection