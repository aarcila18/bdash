@extends('layouts.app',['title' => 'ventas'])

@section('content')
<div class="container p-2 mt-5 mx-auto">
    @livewire('all-sales')
</div>
@endsection
