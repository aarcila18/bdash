@extends('layouts.app',['title' => 'costs'])

@section('content')
<div class="container p-2 mt-5 mx-auto">
    <livewire:all-costs :productionId="null">
</div>
@endsection
