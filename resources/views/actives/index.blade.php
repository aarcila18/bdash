@extends('layouts.app',['title' => 'activos'])

@section('content')
<div class="container p-2 mt-5 mx-auto">
    @livewire('all-actives')
</div>
@endsection
