@extends('layouts.app')

@section('content')
<div class="container p-2 mt-5 mx-auto">
    <livewire:observation-component :id="$active->id" :type="get_class($active)">
</div>
@endsection