<div>
    @include("components.alerts")
    <div class="row py-5 text-right">
        <a class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded" href="{{ route('outgoings.export') }}" target="_blank">Exportar</a>
    </div>
    @include("components.search")
    <div x-data="{ open: false }">
        <button @click="open = true" x-show="!open" class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded">+ Añadir Gasto</button>
        <div id="add-form" class="mb-5 p-10 bg-gray-300 rounded-lg" x-show.transition.duration.300ms="open" @click.away="open = false">
            <h2 class="nunito text-md text-bold mb-5">Crear Gasto:</h2>
            <input wire:model="description" class="bg-gray-200 mb-5 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Descripcion">
            @error('description') <span class="error">{{ $message }}</span> @enderror
            <input wire:model="value" class="bg-gray-200 mb-5 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="valor del gasto">
            @error('value') <span class="error">{{ $message }}</span> @enderror
            <button wire:click="create" class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded">
            Crear
            </button>
        </div>
    </div>
    <div>
        <table class="table-auto w-full">
            <thead>
                <tr>
                <th class="px-4 py-2">ID</th>
                <th class="px-4 py-2">DESCRIPTION</th>
                <th class="px-4 py-2">VALUE</th>
                <th class="px-4 py-2">DATE</th>
                <th class="px-4 py-2"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($outgoings as $outgoing)
                    <tr>
                        <td class="border px-4 py-2">{{$outgoing->id}}</td>
                        <td class="border px-4 py-2">{{$outgoing->description}}</td>
                        <td class="border px-4 py-2">{{moneyformat($outgoing->value)}}</td>
                        <td class="border px-4 py-2">{{$outgoing->created_at}}</td>
                        <td class="border px-4 py-2 flex">
                            <a href="{{ route('outgoings.show', $outgoing->id) }}" class="block py-1 md:py-3 pl-1 align-middle text-gray-600 no-underline hover:text-indigo-400">
                                <i class="fas fa-eye fa-fw mr-3"></i>
                            </a>
                            
                        </td>
                    </tr>          
                @endforeach
                
            </tbody>
        </table>
    </div>
    {{ $outgoings->links() }}
</div>