<div class="my-5 p-8 border-2 border-gray-400 border-dashed bg-gray-200">
@include("components.alerts")

<input type="date" wire:model="start_date">
<input type="date" wire:model="end_date">
<button wire:click="create_report" class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded">
        Crear informe
</button>
    <div>
        <h3>{{count($sales)}} VENTAS</h3>
        <table class="table-auto w-full">
            <thead>
                <tr>
                <th class="px-4 py-2">DATE</th>
                <th class="px-4 py-2">STATUS</th>
                <th class="px-4 py-2">TOTAL</th>
                <th class="px-4 py-2"></th>
                </tr>
            </thead>
            <tbody>
            
                @foreach($sales as $sale)
                    <tr>
                    <td class="border px-4 py-2">{{$sale->woo_date_created}}</td>
                    <td class="border px-4 py-2">{{$sale->status}}</td>
                    <td class="border px-4 py-2">{{moneyformat($sale->total)}}</td>
                    <td class="border px-4 py-2"><a href="{{ route('sales.show', $sale->id) }}">show</a></td>
                    </tr>
                @endforeach
                <tr>
                <td></td>
                <td class="border px-4 py-2">total</td>
                <td class="border px-4 py-2"><h2>{{moneyformat($total_sales)}}</h2></td>
                <td></td>
                </tr>
            </tbody>
        </table>    
    </div>
    <div>
        <h2>{{count($outgoings)}} GASTOS</h2>
        <table class="table-auto w-full">
        <thead>
            <tr>
            <th class="px-4 py-2">DATE</th>
            <th class="px-4 py-2">DESCRIPTION</th>
            <th class="px-4 py-2">VALUE</th>
            <th class="px-4 py-2"></th>
            </tr>
        </thead>
        <tbody>
            @foreach($outgoings as $outgoing)
                <tr>
                    <td class="border px-4 py-2">{{$outgoing->created_at}}</td>
                    <td class="border px-4 py-2">{{$outgoing->description}}</td>
                    <td class="border px-4 py-2">{{moneyformat($outgoing->value)}}</td>
                    <td class="border px-4 py-2"><a href="{{ route('outgoings.show', $outgoing->id) }}">show</a></td>
                </tr>          
            @endforeach
            <tr>
                <td></td>
                <td class="border px-4 py-2">total</td>
                <td class="border px-4 py-2"><h2>{{moneyformat($total_outgoings)}}</h2></td>
            </tr>
        </tbody>
    </table>
    </div>

    <div>
        <h3>{{count($sales_processing)}} VENTAS POR COBRAR</h3>
        <table class="table-auto w-full">
            <thead>
                <tr>
                <th class="px-4 py-2">DATE</th>
                <th class="px-4 py-2">STATUS</th>
                <th class="px-4 py-2">TOTAL</th>
                <th class="px-4 py-2"></th>
                </tr>
            </thead>
            <tbody>
            
                @foreach($sales_processing as $sale)
                    <tr>
                    <td class="border px-4 py-2">{{$sale->woo_date_created}}</td>
                    <td class="border px-4 py-2">{{$sale->status}}</td>
                    <td class="border px-4 py-2">{{moneyformat($sale->total)}}</td>
                    <td class="border px-4 py-2"><a href="{{ route('sales.show', $sale->id) }}">show</a></td>
                    </tr>
                @endforeach
                <tr>
                <td></td>
                <td class="border px-4 py-2">total</td>
                <td class="border px-4 py-2"><h2>{{moneyformat($total_sales_processing)}}</h2></td>
                <td></td>
                </tr>
            </tbody>
        </table>    
    </div>

    <h2>Observaciones:</h2>
    <div>
        @foreach($observations as $observation)
            <span>{{$observation->created_at}}</span>
            <p>{{$observation->user->name}}: {{$observation->description}}</p>
        @endforeach
        {{ $observations->links() }}
    </div>
    <h2>Redes sociales:</h2>
    <h2>GA:</h2>

</div>


