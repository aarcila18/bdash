<div x-data="{ modal: false }">
<div class="row py-5 text-right">
    <a class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded" href="{{ route('tasks.export',$board_id) }}" target="_blank">Exportar</a>
</div>
<!--Modal-->
<div x-show.transition.duration.600ms="modal"  class="modal fixed w-full h-full top-0 left-0 flex items-center justify-center">
    <div wire:click="select(null)" @click="modal = false" class="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"></div>

    <div style="max-height: 80%;" class="modal-container bg-white w-11/12 md:max-w-2xl mx-auto rounded shadow-lg z-50 overflow-y-auto">

    <div @click="modal = false" class="modal-close absolute top-0 right-0 cursor-pointer flex flex-col items-center mt-4 mr-4 text-white text-sm z-50">
        <svg class="fill-current text-white" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
            <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"></path>
        </svg>
    </div>

        <!-- Add margin if you want to see some of the overlay behind the modal-->
        @if(!is_null($selected))
        <div class="modal-content py-4 text-left px-6" >
            <div class="flex justify-between items-center pb-3">
            <div class="flex">
                <p class="text-2xl font-bold">{{$selected->name}}</p>
            </div>
            <div class="flex">
                <select wire:model.lazy="status" wire:change.lazy="edit" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                        <option value="" disable>Estado</option>
                        <option>todo</option>
                        <option>doing</option>
                        <option>review</option>
                        <option>done</option>
                </select>
            </div>
        </div>

        <!--Body-->
        <textarea wire:model.lazy="description" wire:change.lazy="edit" name="" id="" class="w-full p-5" >{{$selected->description}}</textarea>

        <livewire:observation-component :id="$selected->id" :type="get_class($selected)">
        
        </div>
        @endif
    </div>
</div>


<div class="bg-blue w-full font-sans">
            <div class="flex m-4 justify-between">
                <div class="flex">
                    <h3 class="text-gray-600 mr-4">Tablero</h3>
                </div>
            </div>
            <div class="flex px-4 pb-8 items-start overflow-x-scroll">
                <div class="rounded bg-gray-400  flex-no-shrink w-64 p-2 mr-3">
                    <div class="flex justify-between py-1">
                        <h3 class="text-sm">Todo</h3>
                        <svg class="h-4 fill-current text-gray-dark cursor-pointer" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M5 10a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4zm7 0a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4zm7 0a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4z"/></svg>
                    </div>
                    <div class="text-sm mt-2">
                        @foreach($todos as $card)
                        <div @click="modal = true" wire:click="select({{$card->id}})" class="bg-white p-2 rounded mt-1 border-b border-gray cursor-pointer hover:bg-gray-500">
                            {{$card->name}}
                        </div>
                        @endforeach
                        <div x-data="{ add_card: false }">
                            <p x-show="!add_card" class="mt-3 text-gray-dark" @click="add_card = true">Añadir tarea...</p>
                            <div x-show.transition.duration.300ms="add_card" @click.away="add_card = false" class="bg-white p-2 rounded mt-1 border-b border-gray cursor-pointer">
                                <textarea wire:model.lazy="title" name="" id="" rows="5" class="w-full p-3"></textarea>
                                <button @click="add_card = false" wire:click="create('todo')" class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded">+ Añadir</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="rounded bg-gray-400 flex-no-shrink w-64 p-2 mr-3">
                    <div class="flex justify-between py-1">
                        <h3 class="text-sm">Doing</h3>
                        <svg class="h-4 fill-current text-gray-dark cursor-pointer" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M5 10a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4zm7 0a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4zm7 0a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4z"/></svg>
                    </div>
                    <div class="text-sm mt-2">
                        @foreach($doings as $card)
                        <div @click="modal = true" wire:click="select({{$card->id}})" class="bg-white p-2 rounded mt-1 border-b border-gray cursor-pointer hover:bg-gray-500">
                            {{$card->name}}
                        </div>
                        @endforeach
                        <div x-data="{ add_card: false }">
                            <p x-show="!add_card" class="mt-3 text-gray-dark" @click="add_card = true">Añadir tarea...</p>
                            <div x-show.transition.duration.300ms="add_card" @click.away="add_card = false" class="bg-white p-2 rounded mt-1 border-b border-gray cursor-pointer">
                                <textarea wire:model.lazy="title" name="" id="" rows="5" class="w-full p-3"></textarea>
                                <button @click="add_card = false" wire:click="create('doing')" class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded">+ Añadir</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="rounded bg-gray-400 flex-no-shrink w-64 p-2 mr-3">
                    <div class="flex justify-between py-1">
                        <h3 class="text-sm">Review</h3>
                        <svg class="h-4 fill-current text-gray-dark cursor-pointer" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M5 10a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4zm7 0a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4zm7 0a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4z"/></svg>
                    </div>
                    <div class="text-sm mt-2">
                        @foreach($reviews as $card)
                        <div @click="modal = true" wire:click="select({{$card->id}})" class="bg-white p-2 rounded mt-1 border-b border-gray cursor-pointer hover:bg-gray-500">
                            {{$card->name}}
                        </div>
                        @endforeach
                        <div x-data="{ add_card: false }">
                            <p x-show="!add_card" class="mt-3 text-gray-dark" @click="add_card = true">Añadir tarea...</p>
                            <div x-show.transition.duration.300ms="add_card" @click.away="add_card = false" class="bg-white p-2 rounded mt-1 border-b border-gray cursor-pointer">
                                <textarea wire:model.lazy="title" name="" id="" rows="5" class="w-full p-3"></textarea>
                                <button @click="add_card = false" wire:click="create('review')" class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded">+ Añadir</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="rounded bg-gray-400 flex-no-shrink w-64 p-2 mr-3">
                    <div class="flex justify-between py-1">
                        <h3 class="text-sm">Done</h3>
                        <svg class="h-4 fill-current text-gray-dark cursor-pointer" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M5 10a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4zm7 0a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4zm7 0a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4z"/></svg>
                    </div>
                    <div class="text-sm mt-2">
                        @foreach($dones as $card)
                        <div @click="modal = true" wire:click="select({{$card->id}})" class="bg-white p-2 rounded mt-1 border-b border-gray cursor-pointer hover:bg-gray-500">
                            {{$card->name}}
                        </div>
                        @endforeach
                        <div x-data="{ add_card: false }">
                            <p x-show="!add_card" class="mt-3 text-gray-dark" @click="add_card = true">Añadir tarea...</p>
                            <div x-show.transition.duration.300ms="add_card" @click.away="add_card = false" class="bg-white p-2 rounded mt-1 border-b border-gray cursor-pointer">
                                <textarea wire:model.lazy="title" name="" id="" rows="5" class="w-full p-3"></textarea>
                                <button @click="add_card = false" wire:click="create('done')" class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded">+ Añadir</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
