<div>
@include("components.alerts")
@include("components.search")
<div>
<div id="add-form" class="mb-5">
    <input wire:model="name" class="bg-gray-200 mb-5 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Nombre">
    <input wire:model="description" class="bg-gray-200 mb-5 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Descripcion">
    <button wire:click="create" class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded">
    Crear
    </button>
</div>

<table class="table-auto w-full">
        <thead>
            <tr>
            <th class="px-4 py-2">ID</th>
            <th class="px-4 py-2">NAME</th>
            <th class="px-4 py-2"></th>
            </tr>
        </thead>
        <tbody>
            @foreach($campaings as $campaing)
                <tr>
                    <td class="border px-4 py-2">{{$campaing->id}}</td>
                    <td class="border px-4 py-2">{{$campaing->name}}</td>
                    <td class="border px-4 py-2 flex">
                        <a href="{{ route('campaings.show', $campaing->id) }}" class="block py-1 md:py-3 pl-1 align-middle text-gray-600 no-underline hover:text-indigo-400">
                            <i class="fas fa-eye fa-fw mr-3"></i>
                        </a>

                    </td>
                </tr>          
            @endforeach
            
        </tbody>
    </table>
    {{ $campaings->links() }}
</div>
</div>
