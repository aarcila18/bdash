<div class="mt-5 p-10 bg-gray-300 rounded-lg">
    @include("components.alerts")
    <livewire:attachment-component :id="$observable_id" :type="$observable_type">
    <h2 class="relative uppercase text-md nunito font-bold">Comentarios:</h2>
    <textarea rows="4" wire:model="comment" class="max-w-screen-sm bg-gray-200 mb-5 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Deja un comentario"></textarea>
    @error('comment') <span class="error">{{ $message }}</span> @enderror
    <div class="row mb-10">
        <button wire:click="create_comment" class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded">
                Crear
        </button>
    </div>
    <div class="row">
        @foreach($observations as $observation)
        <div class="mt-5">
            <p>• {{$observation->description}}</p>
            <span class="text-gray-500 font-bold text-xs">{{$observation->user->name}} • {{$observation->created_at}}</span>
            
        </div>
        @endforeach
    </div>
</div>
