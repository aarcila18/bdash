<div>
@include("components.alerts")
<div class="row py-5 text-right">
    <a class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded" href="" target="_blank">Exportar</a>
</div>
@include("components.search")
<div>
<div x-data="{ open: false }">
    <button @click="open = true" x-show="!open" class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded">+ Añadir Material</button>
    <div id="add-form" class="mb-5 p-10 bg-gray-300 rounded-lg" x-show.transition.duration.300ms="open" @click.away="open = false">
        <h2 class="nunito text-md text-bold mb-5">Crear material:</h2>
        <input wire:model="name" class="bg-gray-200 mb-5 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Nombre">
        @error('name') <span class="error">{{ $message }}</span> @enderror
        <input wire:model="cost" class="bg-gray-200 mb-5 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Costo por cantidad">
        @error('cost') <span class="error">{{ $message }}</span> @enderror
        <input wire:model="quantity" class="bg-gray-200 mb-5 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="cantidad">
        @error('quantity') <span class="error">{{ $message }}</span> @enderror
        <select wire:model="provider_id" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
            <option value="" disable>Proveedor</option>
            @foreach($providers as $provider)
            <option value="{{$provider->id}}">{{$provider->name}}</option>
            @endforeach
        </select>
        @error('provider_id') <span class="error">{{ $message }}</span> @enderror
        <button wire:click="create" class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded">
        Crear
        </button>
        <div class="row pt-5">
            <span class="text-sm text-gray-600">* Añade una produccion para empezar a añadirle productos</span>
        </div>
     </div>
</div>

<table class="table-auto w-full">
        <thead>
            <tr>
            <th class="px-4 py-2">ID</th>
            <th class="px-4 py-2">NAME</th>
            <th class="px-4 py-2"></th>
            </tr>
        </thead>
        <tbody>
            @foreach($materials as $material)
                <tr>
                    <td class="border px-4 py-2">{{$material->id}}</td>
                    <td class="border px-4 py-2">{{$material->name}}</td>
                    <td class="border px-4 py-2 flex">
                        <a href="{{ route('materials.show', $material->id) }}" class="block py-1 md:py-3 pl-1 align-middle text-gray-600 no-underline hover:text-indigo-400">
                            <i class="fas fa-eye fa-fw mr-3"></i>
                        </a>

                    </td>
                </tr>          
            @endforeach
            
        </tbody>
    </table>
    {{ $materials->links() }}
</div>
</div>
