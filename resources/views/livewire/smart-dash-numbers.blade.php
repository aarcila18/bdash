<div class="w-full">
    <div class="row p-6">
        <h2>Periodo: </h2>
        <label class="block text-gray-700 text-sm font-bold mb-2" for="start_date">
            Inicio
        </label>
        <input type="date" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="start_date" wire:model="start_date">
        <label class="block text-gray-700 text-sm font-bold mb-2" for="end_date">
            final
        </label>
        <input type="date" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="end_date" wire:model="end_date">
    </div>
    @foreach($numbers as $number)
        @include("components.dash-number",["number"=>$number["data"],"subdata"=>$number["subdata"],"title"=>$number["title"],"icon"=>$number["icon"]])   
    @endforeach
</div>
