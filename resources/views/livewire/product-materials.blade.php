<div>
    <select wire:model="material_id" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
            <option value="" disable>Material</option>
            @foreach($all_materials as $material)
                <option value="{{$material->id}}">{{$material->name}}</option>
            @endforeach
    </select>
    @error('material_id') <span class="error">{{ $message }}</span> @enderror
    <input wire:model="quantity" class="bg-gray-200 mb-5 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Cantidad">
    @error('quantity') <span class="error">{{ $message }}</span> @enderror
    <button wire:click="create" class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded">
            Crear
    </button>

    <div>
        @if(!is_null($product))
        <table class="table-auto w-full">
            <thead>
                <tr>
                <th class="px-4 py-2">NAME</th>
                <th class="px-4 py-2">QUANTITY</th>
                <th class="px-4 py-2">TOTAL</th>
                <th class="px-4 py-2"></th>
                </tr>
            </thead>
            <tbody>
            @foreach($product->materials as $material)
            <tr>
                <td class="border px-4 py-2">{{$material->name}}</td>
                <td class="border px-4 py-2">
                    @if($material->pivot->id==$selectid)
                    <input type="text" wire:model="new_quantity">
                    @else
                    {{$material->pivot->material_quantity}}
                    @endif 
                </td>
                <td class="border px-4 py-2">{{moneyformat($material->pivot->material_cost)}}</td>
                <td class="border px-4 py-2">
                @if($selectid==null)
                <i wire:click="select({{$material->pivot->id}})" class="fas fa-edit fa-fw mr-3"></i>
                @endif 
                @if($material->pivot->id==$selectid)
                <i wire:click="updateMaterial" class="fas fa-check-circle fa-fw mr-3"></i>
                <i wire:click="select(null)" class="fas fa-times-circle fa-fw mr-3"></i>
                <i wire:click="deleteMaterial" class="fas fa-trash fa-fw mr-3"></i>
                @endif
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
        @endif
        <p>Total Materiales = {{moneyformat($total)}}</p>
    </div>
</div>
