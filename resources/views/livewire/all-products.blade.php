<div>
@include("components.alerts")
<div class="row py-5 text-right">
    <a class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded" wire:click="updateProducts" wire:loading.remove>obetener productos</a>
    <a class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded" href="{{ route('products.export') }}" target="_blank">Exportar</a>
</div>
@include("components.search")
<livewire:create-product :data="null" :model="'products'">
<select wire:model="status" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
    <option value="" disable>Estado producto</option>
    <option>production</option>
    <option>online</option>
    <option>hold</option>
</select>
<span wire:loading wire:target="updateProducts">cargando productos de woo</span>
@include("components.products-table",['page' => 'products'])
{{ $products->links() }}
</div>
