<div x-data="{modal:false,delete_model:false}">
    @can("userEdit")
    <div class="my-6 align-middle">
        <a @click="modal=true" wire:click="query" class="bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-3 my-5 mx-3 rounded-full cursor-pointer">
            <i class="fas fa-edit fa-fw mr-3 ml-3"></i>
        </a>
    </div>
    @endcan
    <div x-show.transition.duration.600ms="modal" class="modal fixed w-full h-full top-0 left-0 flex items-center justify-center z-50">
        <div @click="modal = false" class="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"></div>
        <div class="modal-container bg-white w-11/12 md:max-w-2xl mx-auto rounded shadow-lg z-50 overflow-y-auto">
            <div class="modal-close absolute top-0 right-0 cursor-pointer flex flex-col items-center mt-4 mr-4 text-white text-sm z-50">
                <svg class="fill-current text-white" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                    <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"></path>
                </svg>
            </div>

            <div class="modal-content py-4 text-left px-6">
                <!-- formulario edit -->
                @include("components.alerts")
                @can("userDelete")
                <a @click="delete_model = true" x-show.transition.duration.600ms="!delete_model" class="block py-1 md:py-3 pl-1 align-middle text-gray-600 no-underline hover:text-indigo-400">
                    <i class="fas fa-trash fa-fw mr-3"></i>
                </a>
                <div x-show.transition.duration.600ms="delete_model" class="my-5">
                <span>Seguro lo quieres borrar <a wire:click="delete" class="bg-red-500 hover:bg-red-700 text-white font-bold p-3 my-5 mx-3 rounded-full cursor-pointer">SI</a><a @click="delete_model = false" class="bg-indigo-500 hover:bg-indigo-700 text-white font-bold p-3 my-5 mx-3 rounded-full cursor-pointer">NO</a></span>
                </div>
                @endcan
                <h2 class="nunito text-md text-bold mb-5">Editando:</h2>
                @foreach($fields as $field)
                <label for="{{$field['name'].$editable_id.'_form_field'}}" class="block text-gray-700 text-sm font-bold mb-2">{{$field['lable']}}</label>
                <input id="{{$field['name'].$editable_id.'_form_field'}}" type="{{$field['type']}}" placeholder="{{$field['name']}}" name="{{$field['name']}}" wire:model="fields.{{$loop->index}}.value" class="bg-gray-200 mb-5 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="">
                @endforeach
                <button wire:click="save" class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded">
                    Editar
                </button>
                <!-- formulario edit -->
            </div>
        </div>
    </div>
</div>