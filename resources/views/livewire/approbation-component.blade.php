<div>
@include("components.alerts")
    @can('approve')
        @if($approbations->count() < $goal)
            @if($myapprobations->count()<1)
                {{$approbations->count()}}
                <i class="fas fa-thumbs-up fa-fw mr-3 " wire:click="like"></i>
                {{$noapprobations->count()}}
                <i class="fas fa-thumbs-down fa-fw mr-3 " wire:click="nolike"></i>
            @else
                <h2>Ya diste tu aprobacion</h2>
            @endif
        @else
            <h2>Aprobado</h2>
            <h2>Ya se completo el numero de aprobaciones</h2>
        @endif
    @endcan
</div>
