<div class="right-0 bottom-0 fixed z-50" x-data="{open:false}" @click.away="open=false">
    <div x-show.transition.opacity.duration.300ms="open">
        @foreach($products as $product)
        <div class="my-5 bg-white p-5">
            <p style="min-width:350px;" class="px-2"><i class="fa fa-star fa-fw mr-3 "></i>{{$product->name}} <a class="bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-2 px-5 rounded-full cursor-pointer float-right" href="{{ route('products.show', $product->id) }}">Ver</a></p>
        </div>
        @endforeach
        @foreach($productions as $production)
        <div class="my-5 bg-white p-5">
            <p style="min-width:350px;" class="px-2"><i class="fa fa-industry fa-fw mr-3 "></i>{{$production->name}} <a class="bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-2 px-5 rounded-full cursor-pointer float-right" href="{{ route('productions.show', $production->id) }}">Ver</a></p>
        </div>
        @endforeach
        @foreach($tasks as $task)
        <div class="my-5 bg-white p-5">
            <p style="min-width:350px;" class="px-2"><i class="fa fa-columns fa-fw mr-3 "></i>{{$task->name}} <a class="bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-2 px-5 rounded-full cursor-pointer float-right" href="">Ver</a></p>
        </div>
        @endforeach
    </div>
    <div @click="open=true" style="max-width:300px;" class="float-right bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-2 m-5 rounded-full cursor-pointer">
        <i class="fas fa-search fa-fw mr-3 ml-3"></i>
        <input style="max-width:150px;" x-show.transition.opacity="open" wire:model="search" type="text" class="mr-5 text-white font-bold bg-transparent focus:outline-none hover:bg-indigo-700" placeholder="Buscar ...">
    </div>
</div>
