<div>
    <div class="" style="min-width:300px">
        @foreach($unreadNotifications as $notification)
        <div class="bg-indigo-200 p-5 my-5 ">
            <h2 class="font-bold">{{$notification->data["title"]}} / <button wire:click="markAsRead('{{ $notification->id }}')">Marcar como leido</button></h2>
            <p>{{$notification->data["body"]}}</p>
        </div>
        @endforeach

        @foreach($readNotifications as $notification)
        <div class="p-5 my-5 ">
            <h2 class="font-bold">{{$notification->data["title"]}}</h2>
            <p>{{$notification->data["body"]}}</p>
        </div>
        @endforeach
    </div>
</div>
