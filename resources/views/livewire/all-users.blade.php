<div>
    @include("components.alerts")

    @include("components.search")
    <div x-data="{ open: false }">
        <button @click="open = true" x-show="!open" class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded">+ Añadir Usuario</button>
        <div id="add-form" class="mb-5 p-10 bg-gray-300 rounded-lg" x-show.transition.duration.300ms="open" @click.away="open = false">
            <h2 class="nunito text-md text-bold mb-5">Crear usuario:</h2>
            <input wire:model="name" class="bg-gray-200 mb-5 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Nombre">
            @error('name') <span class="error">{{ $message }}</span> @enderror
            <input wire:model="email" class="bg-gray-200 mb-5 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="email" placeholder="Email">
            @error('email') <span class="error">{{ $message }}</span> @enderror
            <input wire:model="password" class="bg-gray-200 mb-5 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="password" placeholder="Contraseña">
            @error('password') <span class="error">{{ $message }}</span> @enderror
            <select wire:model="role" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                <option value="" disable>Rol</option>
                @foreach($roles as $role)
                <option value="{{$role->id}}">{{$role->name}}</option>
                @endforeach
            </select>
            <button wire:click="create" class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded">
                Crear
            </button>
        </div>
    </div>
    <div>

        <table class="table-auto w-full">
            <thead>
                <tr>
                    <th class="px-4 py-2">ID</th>
                    <th class="px-4 py-2">NAME</th>
                    <th class="px-4 py-2"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                <tr>
                    <td class="border px-4 py-2">{{$user->id}}</td>
                    <td class="border px-4 py-2">{{$user->name}}</td>
                    <td class="border px-4 py-2 flex">
                        <a href="{{ route('users.show', $user->id) }}" class="block py-1 md:py-3 pl-1 align-middle text-gray-600 no-underline hover:text-indigo-400">
                            <i class="fas fa-eye fa-fw mr-3"></i>
                        </a>
                        
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>
        {{ $users->links() }}
    </div>
</div>