<div>
    <select wire:model="workforce_id" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
            <option value="" disable>workforce</option>
            @foreach($all_workforces as $workforce)
                <option value="{{$workforce->id}}">{{$workforce->name}}</option>
            @endforeach
    </select>
    @error('workforce_id') <span class="error">{{ $message }}</span> @enderror
    <input wire:model="quantity" class="bg-gray-200 mb-5 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Cantidad">
    @error('quantity') <span class="error">{{ $message }}</span> @enderror
    <button wire:click="create" class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded">
            Crear
    </button>

    <div>
        @if(!is_null($product))
        <table class="table-auto w-full">
            <thead>
                <tr>
                <th class="px-4 py-2">NAME</th>
                <th class="px-4 py-2">QUANTITY</th>
                <th class="px-4 py-2">TOTAL</th>
                <th class="px-4 py-2"></th>
                </tr>
            </thead>
            <tbody>
            @foreach($product->workforces as $workforce)
            <tr>
                <td class="border px-4 py-2">{{$workforce->name}}</td>
                <td class="border px-4 py-2">
                    @if($workforce->pivot->id==$selectid)
                    <input type="text" wire:model="new_quantity">
                    @else
                    {{$workforce->pivot->workforce_quantity}}
                    @endif 
                </td>
                <td class="border px-4 py-2">{{moneyformat($workforce->pivot->workforce_cost)}}</td>
                <td class="border px-4 py-2">
                @if($selectid==null)
                <i wire:click="select({{$workforce->pivot->id}})" class="fas fa-edit fa-fw mr-3"></i>
                @endif 
                @if($workforce->pivot->id==$selectid)
                <i wire:click="updateWorkforce" class="fas fa-check-circle fa-fw mr-3"></i>
                <i wire:click="select(null)" class="fas fa-times-circle fa-fw mr-3"></i>
                <i wire:click="deleteWorkforce" class="fas fa-trash fa-fw mr-3"></i>
                @endif
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
        @endif
        <p>Total workforcees = {{moneyformat($total)}}</p>
    </div>
</div>
