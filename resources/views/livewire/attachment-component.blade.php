<div class="my-10" x-data="{open:false}">
    <h2 class="relative uppercase text-md nunito font-bold">Adjuntos:</h2>
    
    <div>
        @foreach($attachments as $attachment)
        <div class="mt-5" x-show="!open">
            <p>• <a href="{{url('/attachments')."/".$attachment->filename}}">{{$attachment->filename}}</a></p>
            <span class="text-gray-500 font-bold text-xs">{{$attachment->created_at}}</span>
            
        </div>  
        @endforeach
        <button type="submit" @click="open=true" x-show="!open" class="mt-5 bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded">Adjuntar</button>
        <form id="drop-area" x-show.transition.duration.300ms="open" action="{{ route('file.upload.post') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row mt-5 ">
                <div class=" col-md-6 border-2 border-gray-400 border-dashed p-10 max-w-screen-sm rounded-lg text-center">
                    <i style="font-size: 60px;" class="fa fa-file-upload fa-fw fa-inverse text-gray-400"></i>
                    <div class="row mt-3">
                        <input type="file" name="file">
                        <input type="text" name="attachable_id" class="form-control" hidden value="{{$attachable_id}}">
                        <input type="text" name="attachable_type" class="form-control" hidden value="{{$attachable_type}}">
                    </div>
                </div>
                <div class="col-md-6">
                    <button type="submit" class="mt-5 bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded">Subir archivo</button>
                </div>
            </div>
        </form>
    </div>
    <script>
        let dropArea = document.getElementById('drop-area')

        dropArea.addEventListener('dragenter', handlerFunction, false)
        dropArea.addEventListener('dragleave', handlerFunction, false)
        dropArea.addEventListener('dragover', handlerFunction, false)
        dropArea.addEventListener('drop', handlerFunction, false)
    </script>
</div>
