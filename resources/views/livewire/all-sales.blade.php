<div>
@include("components.alerts")
<div class="row py-5 text-right">
    <a class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded" wire:click="updateSales" wire:loading.remove>obetener ventas</a>
    <a class="bg-indigo-400 hover:bg-indigo-600 text-white font-bold py-2 px-4 rounded" href="{{ route('sales.export') }}" target="_blank">Exportar</a>
</div>
@include("components.search")
    
    <span wire:loading wire:target="updateSales">cargando ventas de woo</span>
    <table class="table-auto w-full">
        <thead>
            <tr>
            <th class="px-4 py-2">SKU</th>
            <th class="px-4 py-2">STATUS</th>
            <th class="px-4 py-2">TOTAL</th>
            <th class="px-4 py-2"></th>
            </tr>
        </thead>
        <tbody>
        
            @foreach($sales as $sale)
                <tr>
                    <td class="border px-4 py-2">{{$sale->woo_id}}</td>
                    <td class="border px-4 py-2">{{$sale->status}}</td>
                    <td class="border px-4 py-2">{{moneyformat($sale->total)}}</td>
                    <td class="border px-4 py-2 flex">
                        <a href="{{ route('sales.show', $sale->id) }}" class="block py-1 md:py-3 pl-1 align-middle text-gray-600 no-underline hover:text-indigo-400">
                            <i class="fas fa-eye fa-fw mr-3"></i>
                        </a>
                        
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $sales->links() }}

</div>
