<div class="h-40 lg:h-20 w-full flex flex-wrap">
<nav id="header" class="bg-gray-400 w-full lg:max-w-sm flex items-center border-b-1 border-gray-300 order-2 order-1">
<img src="{{brand_logo()}}" class="w-1/2 self-center m-auto" alt="">
</nav>
<nav id="header1" class="bg-gray-100 w-auto flex-1 border-b-1 border-gray-300 order-1 order-2">
<div class=" h-full items-center w-full">

<div class="flex relative items-center h-full inline-block pr-6 float-left">
@if(! empty($title))<h2 class="relative ml-8 p-3 uppercase text-md nunito font-bold">{{$title}}</h2> @endif
</div>

    <!--Menu-->

    <div class=" flex relative items-center h-full inline-block pr-6 float-right">

        <div class="relative text-sm">
            <button id="userButton" class="flex items-center focus:outline-none mr-3">
                <span class="hidden md:inline-block">Hola, <span style="font-weight:bold;">{{ Auth::user()->name }} </span>@if(Auth::user()->unreadNotifications->count()>0)<span class="rounded-full bg-indigo-500 text-white px-2 py-1">{{Auth::user()->unreadNotifications->count()}} <i class="fa fa-bell fa-fw fa-inverse text-white-500"></i></span>@endif</span>
                <svg class="pl-2 h-2" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129">
                    <g>
                        <path d="m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z"></path>
                    </g>
                </svg>
            </button>
            <div id="userMenu" class="bg-white nunito rounded shadow-md mt-2 absolute mt-12 top-0 right-0 min-w-full overflow-auto z-30 invisible">
                <ul class="list-reset">
                    <li><a href="#" class="px-4 py-2 block text-gray-900 hover:bg-indigo-400 hover:text-white no-underline hover:no-underline">Mi cuenta</a></li>
                    <li>
                        <a href="/notifications" class="px-4 py-2 block text-gray-900 hover:bg-indigo-400 hover:text-white no-underline hover:no-underline">Notificaciones</a>
                    </li>
                    <li>
                        <hr class="border-t mx-2 border-gray-400">
                    </li>
                    <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="px-4 py-2 block text-gray-900 hover:bg-indigo-400 hover:text-white no-underline hover:no-underline">Salir</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
        </div>

    </div>

    <!-- / Menu -->

</div>

</nav>
</div>      