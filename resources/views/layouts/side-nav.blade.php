<!-- Side bar-->
    <div x-data="{submenu:''}" @click.away="submenu=''" id="sidebar" class="bg-indigo-700 h-screen w-16 menu bg-white text-white flex items-center nunito static fixed">
        <span class="absolute text-gray-700" style="top:10px;left:10px;font-size:9px;">BDash 0.4 Alpha</span>
        <ul class="list-reset">
            @can("accessAdmin")
            <li class="px-4 my-2 md:my-0 w-full">
                <a @click="submenu='admin'" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-indigo-200">
                    <i class="fas fa-cog fa-fw mr-3 {{ (Route::is('admin') ? 'text-indigo-400' : '') }}"></i><span class="w-full inline-block pb-1 md:pb-0 text-sm">Admin Panel</span>
                </a>
            </li>
            <li class="px-4 my-2 md:my-0 w-full bg-indigo-500">
                <a href="/admin" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-indigo-200" x-show.transition.opacity.duration.300ms="submenu==='admin'">
                    <i class="fas fa-user-cog fa-fw mr-3 {{ (Route::is('admin.*') ? 'text-indigo-400' : '') }}"></i><span class="w-full inline-block pb-1 md:pb-0 text-sm">Config</span>
                </a>
            </li>
            <li class="px-4 my-2 md:my-0 w-full bg-indigo-500">
                <a href="/admin/users" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-indigo-200" x-show.transition.opacity.duration.300ms="submenu==='admin'">
                    <i class="fas fa-user fa-fw mr-3 {{ (Route::is('admin.*') ? 'text-indigo-400' : '') }}"></i><span class="w-full inline-block pb-1 md:pb-0 text-sm">Usuarios</span>
                </a>
            </li>
            @endcan
            @can("accessCompany")
            <li class="px-4 my-2 md:my-0 w-full">
                <a href="/dashboard" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-indigo-200">
                    <i class="fas fa-chart-bar fa-fw mr-3 {{ (Route::is('dashboard') ? 'text-indigo-400' : '') }}"></i><span class="w-full inline-block pb-1 md:pb-0 text-sm">Dashboard</span>
                </a>
            </li>
            <li class="px-4 my-2 md:my-0 w-full">
                <a href="/campaings" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-indigo-200">
                    <i class="fas fa-tasks fa-fw mr-3 {{ (Route::is('campaings.*') ? 'text-indigo-400' : '') }}"></i><span class="w-full inline-block pb-1 md:pb-0 text-sm">Campañas</span>
                </a>
            </li>
            <li class="px-4 my-2 md:my-0 w-full">
                <a href="/products" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-indigo-200">
                    <i class="fa fa-star fa-fw mr-3 {{ (Route::is('products.*') ? 'text-indigo-400' : '') }}"></i><span class="w-full inline-block pb-1 md:pb-0 text-sm">Productos</span>
                </a>
            </li>
            <li class="px-4 my-2 md:my-0 w-full">
                <a href="/sales" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-indigo-200">
                    <i class="fas fa-money-bill-wave fa-fw mr-3 {{ (Route::is('sales.*') ? 'text-indigo-400' : '') }}"></i><span class="w-full inline-block pb-1 md:pb-0 text-sm">Ventas</span>
                </a>
            </li>
            <li class="px-4 my-2 md:my-0 w-full">
                <a href="/outgoings" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-indigo-200">
                    <i class="fa fa-wallet fa-fw mr-3 {{ (Route::is('outgoings.*') ? 'text-indigo-400' : '') }}"></i><span class="w-full inline-block pb-1 md:pb-0 text-sm">Gastos</span>
                </a>
            </li>
            <!-- Balance group -->
            <li class="px-4 my-2 md:my-0 w-full">
                <a @click="submenu='balance'" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-indigo-200">
                    <i class="fas fa-balance-scale fa-fw mr-3 {{ (Route::is('actives') ? 'text-indigo-400' : '') }}"></i><span class="w-full inline-block pb-1 md:pb-0 text-sm">Balance</span>
                </a>
            </li>
            <li class="px-4 my-2 md:my-0 w-full bg-indigo-500">
                <a href="/passives" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-indigo-200" x-show.transition.opacity.duration.300ms="submenu==='balance'">
                    <i class="fas fa-arrow-up fa-fw mr-3 {{ (Route::is('actives.*') ? 'text-indigo-400' : '') }}"></i><span class="w-full inline-block pb-1 md:pb-0 text-sm">Activos</span>
                </a>
            </li>
            <li class="px-4 my-2 md:my-0 w-full bg-indigo-500">
                <a href="/passives" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-indigo-200" x-show.transition.opacity.duration.300ms="submenu==='balance'">
                    <i class="fas fa-arrow-down fa-fw mr-3 {{ (Route::is('passives.*') ? 'text-indigo-400' : '') }}"></i><span class="w-full inline-block pb-1 md:pb-0 text-sm">Pasivos</span>
                </a>
            </li>
            <!-- grupo de producciones -->
            <li class="px-4 my-2 md:my-0 w-full">
                <a @click="submenu='productions'" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-indigo-200">
                    <i class="fas fa-warehouse fa-fw mr-3 {{ (Route::is('productions.*') ? 'text-indigo-400' : '') }}"></i><span class="w-full inline-block pb-1 md:pb-0 text-sm">Produccion</span>
                </a>
            </li>
            <li class="px-4 my-2 md:my-0 bg-indigo-500" x-show.transition.opacity.duration.300ms="submenu==='productions'">
                <a href="/productions" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-indigo-200">
                    <i class="fas fa-industry fa-fw mr-3 {{ (Route::is('productions.*') ? 'text-indigo-400' : '') }}"></i><span class="w-full inline-block pb-1 md:pb-0 text-sm">Producciones</span>
                </a>
            </li>
            <li class="px-4 my-2 md:my-0 bg-indigo-500" x-show.transition.opacity.duration.300ms="submenu==='productions'">
                <a href="/providers" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-indigo-200">
                    <i class="fas fa-truck fa-fw mr-3 {{ (Route::is('providers.*') ? 'text-indigo-400' : '') }}"></i><span class="w-full inline-block pb-1 md:pb-0 text-sm">Proveedores</span>
                </a>
            </li>
            <li class="px-4 my-2 md:my-0 bg-indigo-500" x-show.transition.opacity.duration.300ms="submenu==='productions'">
                <a href="/materials" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-indigo-200">
                    <i class="fas fa-cubes fa-fw mr-3 {{ (Route::is('materials.*') ? 'text-indigo-400' : '') }}"></i><span class="w-full inline-block pb-1 md:pb-0 text-sm">Materia prima</span>
                </a>
            </li>
            <li class="px-4 my-2 md:my-0 bg-indigo-500" x-show.transition.opacity.duration.300ms="submenu==='productions'">
                <a href="/workforces" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-indigo-200">
                    <i class="fas fa-users-cog fa-fw mr-3 {{ (Route::is('workforces.*') ? 'text-indigo-400' : '') }}"></i><span class="w-full inline-block pb-1 md:pb-0 text-sm">Mano de obra</span>
                </a>
            </li>
            <li class="px-4 my-2 md:my-0 bg-indigo-500" x-show.transition.opacity.duration.300ms="submenu==='productions'">
                <a href="/costs" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-indigo-200">
                    <i class="fas fa-comment-dollar fa-fw mr-3 {{ (Route::is('costs.*') ? 'text-indigo-400' : '') }}"></i><span class="w-full inline-block pb-1 md:pb-0 text-sm">Costos</span>
                </a>
            </li>
            <!-- grupo de producciones -->
            @endcan
            <li class="px-4 my-2 md:my-0 w-full">
                <a href="/clients" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-indigo-200">
                    <i class="fas fa-user fa-fw mr-3 {{ (Route::is('clients.*') ? 'text-indigo-400' : '') }}"></i><span class="w-full inline-block pb-1 md:pb-0 text-sm">Clientes</span>
                </a>
            </li>
            <li class="px-4 my-2 md:my-0 w-full">
                <a href="/tasks" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-indigo-200">
                    <i class="fas fa-columns fa-fw mr-3 {{ (Route::is('tasks.*') ? 'text-indigo-400' : '') }}"></i><span class="w-full inline-block pb-1 md:pb-0 text-sm">Tareas</span>
                </a>
            </li>
        </ul>
    <img src="https://upload.wikimedia.org/wikipedia/commons/f/f5/Circle_file.487.png" class="w-10 m-3 absolute bottom-0 left-0" alt="">
    </div>
    <livewire:search-models>