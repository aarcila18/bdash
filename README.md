
## Requerimientos

- Laravel charts (https://charts.erik.cat/)
- Laravel excel (https://laravel-excel.com/)
- Laravel dompdf (https://github.com/barryvdh/laravel-dompdf)
- Laravel-pwa (https://github.com/silviolleite/laravel-pwa)
