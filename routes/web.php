<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes(['register' => false]);


Route::group(['middleware' => 'auth'], function() {
    Route::group(['middleware' => 'can:accessData'], function() {
        Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

        Route::get('/campaings', 'CampaingController@index')->name('campaings.index');
        Route::get('/campaings/{id}', 'CampaingController@show')->name('campaings.show');

        Route::get('/products', 'ProductController@index')->name('products.index');
        Route::get('/products/export', 'ProductController@export')->name('products.export');
        Route::get('/products/{id}', 'ProductController@show')->name('products.show');

        Route::get('/sales', 'SaleController@index')->name('sales.index');
        Route::get('/sales/export', 'SaleController@export')->name('sales.export');
        Route::get('/sales/{id}', 'SaleController@show')->name('sales.show');

        Route::get('/outgoings', 'OutgoingController@index')->name('outgoings.index');
        Route::get('/outgoings/export', 'OutgoingController@export')->name('outgoings.export');
        Route::get('/outgoings/{id}', 'OutgoingController@show')->name('outgoings.show');

        Route::get('/clients', 'ClientController@index')->name('clients.index');
        Route::get('/clients/export', 'ClientController@export')->name('clients.export');
        Route::get('/clients/{id}', 'ClientController@show')->name('clients.show');

        Route::get('/productions', 'ProductionController@index')->name('productions.index');
        Route::get('/productions/export', 'ProductionController@export')->name('productions.export');
        Route::get('/productions/export/{id}', 'ProductionController@exportSingle')->name('productions.single.export');
        Route::get('/productions/{id}', 'ProductionController@show')->name('productions.show');

        Route::get('/providers', 'ProviderController@index')->name('providers.index');
        Route::get('/providers/export', 'ProviderController@export')->name('providers.export');
        Route::get('/providers/{id}', 'ProviderController@show')->name('providers.show');

        Route::get('/materials', 'MaterialController@index')->name('materials.index');
        Route::get('/materials/{id}', 'MaterialController@show')->name('materials.show');

        Route::get('/workforces', 'WorkforceController@index')->name('workforces.index');
        Route::get('/workforces/{id}', 'WorkforceController@show')->name('workforces.show');

        Route::get('/passives', 'PassiveController@index')->name('passives.index');
        Route::get('/passives/{id}', 'PassiveController@show')->name('passives.show');

        Route::get('/actives', 'ActiveController@index')->name('actives.index');
        Route::get('/actives/{id}', 'ActiveController@show')->name('actives.show');

        Route::get('/costs', 'CostController@index')->name('costs.index');
        Route::get('/costs/export', 'CostController@export')->name('costs.export');
        Route::get('/costs/{id}', 'CostController@show')->name('costs.show');

        Route::get('/tasks/export/{id}', 'TaskController@export')->name('tasks.export');
    });

    Route::get('/tasks', 'TaskController@index')->name('tasks.index');
    Route::post('file-upload', 'AttachmentController@fileUploadPost')->name('file.upload.post');
    Route::get('/notifications', 'NotificationController@index')->name('notifications.index');

    // admin routes
    Route::group(['middleware' => 'can:accessData'], function() {
        Route::get('/admin', 'AdminController@index')->name('admin');

        Route::get('/admin/users', 'UserController@index')->name('users.index');
        Route::get('/admin/users/{id}', 'UserController@show')->name('users.show');
    });
    
});

Route::get('/offline', function () {    
    return view('vendor/laravelpwa/offline');
});