<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            ConfigSeeder::class,
            RoleSeeder::class,
            UserSeeder::class,
            BoardSeeder::class,
            ProviderSeeder::class,
            MaterialSeeder::class,
            WorkforceSeeder::class,
            ProductionSeeder::class,
            ProductSeeder::class,
            ]);

        // $this->call([
        //     ConfigSeeder::class,
        //     RoleSeeder::class,
        //     UserSeeder::class,
        //     BoardSeeder::class,
        //     ]);
    }
}
