<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = User::create([
            'name' => "andres arcila",
            'email' => "andresarcila2@me.com",
            'password' => Hash::make("12345678"),
        ]);

        $user->roles()->attach(Role::where('name', 'admin')->first());
    }
}
