<?php

use Illuminate\Database\Seeder;
use App\Board;

class BoardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role = new Board();
        $role->name = 'general';
        $role->user_id = 1;
        $role->save();

        $role = new Board();
        $role->name = 'especifica';
        $role->user_id = 1;
        $role->save();
    }
}
