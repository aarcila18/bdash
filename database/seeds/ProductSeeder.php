<?php

use App\MaterialProduct;
use App\Product;
use App\WorkforceProduct;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Product::class, 20)->create();
        factory(MaterialProduct::class, 50)->create();
        factory(WorkforceProduct::class, 50)->create();
    }
}
