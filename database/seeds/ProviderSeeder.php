<?php

use Illuminate\Database\Seeder;
use App\Provider;

class ProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role = new Provider();
        $role->name = 'proveedor 1';
        $role->address = 'cali cra 62c #9-174';
        $role->cel = '55555555';
        $role->save();

        $role = new Provider();
        $role->name = 'proveedor 2';
        $role->address = 'cali cra 62a';
        $role->cel = '55555555';
        $role->save();

        $role = new Provider();
        $role->name = 'proveedor 3';
        $role->address = 'cali cra 62a';
        $role->cel = '55555555';
        $role->save();

        $role = new Provider();
        $role->name = 'proveedor 4';
        $role->address = 'cali cra 62a';
        $role->cel = '55555555';
        $role->save();
    }
}
