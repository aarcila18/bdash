<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Material;
use App\Provider;
use Faker\Generator as Faker;

$factory->define(Material::class, function (Faker $faker) {
    static $number = 1;
    
    $cost = $faker->numberBetween(100000, 1500000);
    $qty = $faker->numberBetween(1, 100);
    return [
        'name' => 'material - '.$number++,
        'cost' => $cost,
        'quantity' => $qty,
        'unit_cost' => $cost/$qty,
        'provider_id' => Provider::all()->random()->id,
    ];
});
