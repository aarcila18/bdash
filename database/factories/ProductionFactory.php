<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Production;
use Faker\Generator as Faker;

$factory->define(Production::class, function (Faker $faker) {
    static $number = 1;

    return [
        'name' => 'produccion - '.$number++,
        'description' => $faker->sentence,
    ];
});
