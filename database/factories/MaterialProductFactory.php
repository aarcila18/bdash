<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Material;
use App\MaterialProduct;
use App\Product;
use Faker\Generator as Faker;

$factory->define(MaterialProduct::class, function (Faker $faker) {
    $material_id = Material::all()->random()->id;
    $material = Material::find($material_id);
    $material_qty = $faker->randomFloat(1,0.5, 4);
    $product_id = Product::all()->random()->id;
    $product = Product::find($product_id);
    $material_cost = $material_qty*$material->unit_cost;
    $product->production_price = $product->production_price+$material_cost;
    $product->save();
    return [
        'material_id' => $material_id,
        'product_id' => $product_id,
        'production_id' => $product->production_id,
        'material_quantity' => $material_qty,
        'material_cost' => $material_cost,
        'quantity' => $product->stock,
    ];
});
