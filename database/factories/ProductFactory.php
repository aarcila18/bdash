<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use App\Production;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    static $number = 1;

    return [
        'sku' => $faker->unique()->bothify('BYP###??'),
        'name' => 'producto dummie - '.$number++,
        'description' => $faker->sentence,
        'stock' => $faker->numberBetween(5, 100),
        'production_id' => Production::all()->random()->id,
    ];
});
