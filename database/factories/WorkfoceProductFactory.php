<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use App\Workforce;
use App\WorkforceProduct;
use Faker\Generator as Faker;

$factory->define(WorkforceProduct::class, function (Faker $faker) {
    $workforce_id = Workforce::all()->random()->id;
    $workforce = Workforce::find($workforce_id);
    $workforce_qty = $faker->randomFloat(1,0.5, 4);
    $product_id = Product::all()->random()->id;
    $product = Product::find($product_id);
    $workforce_cost = $workforce_qty*$workforce->unit_cost;
    $product->production_price = $product->production_price+$workforce_cost;
    $product->save();
    return [
        'workforce_id' => $workforce_id,
        'product_id' => $product_id,
        'production_id' => $product->production_id,
        'workforce_quantity' => $workforce_qty,
        'workforce_cost' => $workforce_cost,
        'quantity' => $product->stock,
    ];
});
