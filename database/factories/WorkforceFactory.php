<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Provider;
use App\Workforce;
use Faker\Generator as Faker;

$factory->define(Workforce::class, function (Faker $faker) {
    static $number = 1;
    
    $cost = $faker->numberBetween(100000, 1500000);
    $qty = $faker->numberBetween(1, 100);
    return [
        'name' => 'mano de obra - '.$number++,
        'cost' => $cost,
        'quantity' => $qty,
        'unit_cost' => $cost/$qty,
        'provider_id' => Provider::all()->random()->id,
    ];
});
