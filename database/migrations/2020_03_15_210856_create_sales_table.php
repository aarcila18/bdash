<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->id();
            $table->integer('woo_id');
            $table->string('status');
            $table->string('order_key');
            $table->string('discount_total');
            $table->string('discount_tax');
            $table->string('shipping_total');
            $table->string('shipping_tax');
            $table->string('cart_tax');
            $table->string('total');
            $table->string('woo_date_created');
            $table->bigInteger('campaing_id')->nullable();
            $table->bigInteger('client_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
