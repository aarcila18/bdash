<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productions', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description')->nullable();
            $table->date('limit_at')->nullable();
            $table->string('total_costs')->nullable();
            $table->string('total_materials')->nullable();
            $table->integer('total_workforces')->nullable();
            $table->string('total_stock')->nullable();
            $table->string('total')->nullable();
            $table->string('budget')->nullable();
            $table->boolean('approved')->default(0);
            $table->enum('status', ['created', 'revision', 'accepted'])->default("created");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productions');
    }
}
