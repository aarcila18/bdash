<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_tracks', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('campaing_id')->unsigned();
            $table->foreign('campaing_id')->references('id')->on('campaings')->onDelete('cascade');
            $table->enum('red', ['facebook', 'instagram','GA'])->default("facebook");
            $table->integer("followers");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social_tracks');
    }
}
