<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('woo_id')->nullable();
            $table->string('sku')->unique();
            $table->enum('type', ['simple', 'variable','variation'])->default("simple");
            $table->enum('status', ['production', 'hold','online'])->default("production");
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('image')->default('proyect.jpg');
            $table->string('woo_link')->nullable();
            $table->integer('stock')->nullable();
            $table->string('production_price')->nullable();
            $table->string('wholesale_price')->nullable();
            $table->string('price')->nullable();
            $table->string('discount_price')->nullable();
            $table->string('contr_margin')->nullable();
            $table->bigInteger('production_id')->nullable();
            $table->string('parent_sku')->nullable();
            $table->string('attribute')->nullable();
            $table->bigInteger('campaing_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
